package canpango.connectors;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import canpango.utiliy.Utility;



public class SSHConnector {
	
	public boolean	verbose;
	
	private Session session;
	private int		localPort;
	
	public SSHConnector () {
		session = null;
		localPort = -1;
		verbose = false;
	}
	
	
	public void openConnection (String hostnameOrIP, String privateKeyFilePathAndName, String username, String password, int requestedLocalPort, int remotePort) throws Exception {
		openConnection (hostnameOrIP, privateKeyFilePathAndName, username, password, requestedLocalPort, remotePort, false);
	}
	public void openConnection (String hostnameOrIP, String privateKeyFilePathAndName, String username, String password, int requestedLocalPort, int remotePort, boolean enableStrictHostKeyChecking) throws Exception {
        
        try {
        	// If there is already a session open, don't make a new one.
        	if (session != null) return;
        	
        	// Start a local session.
        	if (verbose) Utility.printLine("");
        	if (verbose) Utility.printLine("Starting SSH session...");
            JSch jsch = new JSch();
            
            // If a private keyfile is indicated, use that for the local session.
            if (Utility.isNotEmpty(privateKeyFilePathAndName))	jsch.addIdentity(privateKeyFilePathAndName);
            
            // Start the local session.
            int port = 22;
            session = jsch.getSession(username, hostnameOrIP, port);
           
            // Only use a password if no private keyfile was indicated.
            if (Utility.isEmpty(privateKeyFilePathAndName)) session.setPassword(password);
            
            // Enable strict host checking.
            String strictHostKeyChecking = enableStrictHostKeyChecking ? "yes" : "no";
            session.setConfig("StrictHostKeyChecking", strictHostKeyChecking);
            
            // Establish the connection.
            String remoteHost = "localhost";
            if (verbose) Utility.printLine("Establishing connection between "+"localhost:"+requestedLocalPort+" and "+remoteHost+":"+remotePort+". StrictHostKeyChecking is "+ strictHostKeyChecking+".");
            session.connect();
            
            // Report the status and assigned port, if changed.
            int assignedLocalPort = session.setPortForwardingL(requestedLocalPort, remoteHost, remotePort);
            int localPort = assignedLocalPort;
            String changedLocalPortStatus = assignedLocalPort != requestedLocalPort ? " Warning, assigned local port is different from what was requested." : "";
            if (verbose) Utility.printLine("Success, localhost:"+ assignedLocalPort +" is connected to "+ hostnameOrIP +":"+ remotePort + changedLocalPortStatus);
            if (verbose) Utility.printLine("");
        }
        catch (Exception exception) {
        	session = null;
        	localPort = -1;
        	if (verbose) Utility.printLine("Failure. Error and stacktrace follow: ");
        	if (verbose) Utility.printLine(Utility.getDetailedExceptionString(exception));
        	throw new Exception(exception);
        }
	}
	
	
	public void closeConnection () {
		if (session != null) {
			try {
				if (verbose) Utility.printLine("");
				if (verbose) Utility.printLine("Closing SSH connection...");
				
				session.delPortForwardingL(localPort);
				session = null;
				localPort = -1;
				
				if (verbose) Utility.printLine("Closed.");
				if (verbose) Utility.printLine("");
			} 
			catch (JSchException exception) {
				if (verbose) Utility.printLine("Failure, could not close connection, will nullify. Error and stacktrace follow: ");
				if (verbose) Utility.printLine(Utility.getDetailedExceptionString(exception));
				session = null;
				localPort = -1;
			}
		}
		else {
			if (verbose) Utility.printLine("No SSH session to close!");
		}
	}
	
	
	
	public static void closeConnection (int localPort, boolean verbose) {
		try {
			if (verbose) Utility.printLine("");
			if (verbose) Utility.printLine("Manually closing SSH port forwarding...");
			
			JSch jsch = new JSch();
			Session session = jsch.getSession("localhost");
			session.delPortForwardingL(localPort);
			
			if (verbose) Utility.printLine("Port forwarding closed.");
			if (verbose) Utility.printLine("");
		} 
		catch (JSchException e) {
			if (verbose) Utility.printLine("Unable to manually close port forwarding.");
		}
	}
}
