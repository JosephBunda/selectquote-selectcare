package canpango.connectors;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;

import canpango.utiliy.Utility;

public class MySQLConnector {
	
	public 	boolean verbose;
	
	private Connection connection;
	
	
	
	public MySQLConnector () {
		connection = null;
		verbose = false;
	}
	
	public void openConnection (String hostnameOrIP, int port, String schema, String username, String password, boolean establishNewConnection) throws Exception {
		try {
			// Establish a connection.
			if (verbose) Utility.printLine("");
			if (verbose) Utility.printLine("Connecting to MySQL...");
			
			// Only establish a new connection if explicitly asked to. Otherwise, just continue using the existing session.
			if (!establishNewConnection && connection != null) {
				if (verbose) Utility.printLine("A connection already exists, using that.");
				return;
			}
			
			// Build the connection string and connect.
			String connectionString = "jdbc:mysql://"+ hostnameOrIP +":"+port +"/"+ schema +"?user="+ username +"&password="+ password;
			connection = DriverManager.getConnection(connectionString);

			if (verbose) Utility.printLine("Connection established.");
			if (verbose) Utility.printLine("");
		} 
		catch (SQLException exception) {
		    if (verbose) Utility.printLine("Could not establish connection. Error and stacktrace follows: ");
		    if (verbose) Utility.printLine("SQLException: "+ 	exception.getMessage());
		    if (verbose) Utility.printLine("SQLState: "+ 		exception.getSQLState());
		    if (verbose) Utility.printLine("VendorError: "+ 	exception.getErrorCode());
		    if (verbose) Utility.printLine(Utility.getDetailedExceptionString(exception));
		    throw new Exception();
		}
	}
	
	
	
	public void closeConnection () {
		try {
			if (verbose) Utility.printLine("");
			if (verbose) Utility.printLine("Closing MySQL connection....");
			
			connection.close();
			
			if (verbose) Utility.printLine("Connection closed.");
			if (verbose) Utility.printLine("");
		} 
		catch (SQLException exception) {
			if (verbose) Utility.printLine("Failure, could not close connection, will nullify. Error and stacktrace follow: ");
			if (verbose) Utility.printLine(Utility.getDetailedExceptionString(exception));
			connection = null;
		}
	}
	
	
	
	public int executeStatement (String statement) throws Exception {
		if (connection != null) {
			
			try {
				int affectedRowCount = 0;
				PreparedStatement preparedStatement = connection.prepareStatement(statement);
				affectedRowCount = preparedStatement.executeUpdate();
				return affectedRowCount;
			} 
			catch (SQLException e) {
				if (verbose) Utility.printLine("Error executing statement. Stack trace follows: ");
				if (verbose) Utility.printLine(Utility.getDetailedExceptionString(e));
				throw new Exception(e);
			}
			
		}
		else {
			if (verbose) Utility.printLine("Error: You cannot execute statements without a connection. Open a connection to a MySQL database first and try again.");
			throw new Exception();
		}
	}
	
	
	public int executeBatchStatements (String[] statements) throws Exception {
		if (connection != null) {
			
			try {
				Statement batchStatement = connection.createStatement();
				
				for (String sql : statements) {
					
					batchStatement.addBatch(sql);
				}
				
				int[] affectedRowCounts = batchStatement.executeBatch();
				
				int totalAffectedRows = 0;
				for (int i = 0; i < affectedRowCounts.length; i++) {
					totalAffectedRows += affectedRowCounts[i];
				}
				
				return totalAffectedRows;
			} 
			catch (SQLException e) {
				if (verbose) Utility.printLine("Error executing statement. Stack trace follows: ");
				if (verbose) Utility.printLine(Utility.getDetailedExceptionString(e));
				throw new Exception(e);
			}
			
		}
		else {
			if (verbose) Utility.printLine("Error: You cannot execute statements without a connection. Open a connection to a MySQL database first and try again.");
			throw new Exception();
		}
	}
	
}
