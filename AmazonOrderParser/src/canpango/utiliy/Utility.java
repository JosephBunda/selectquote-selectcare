package canpango.utiliy;

import java.util.ArrayList;

public class Utility {
	
	public static boolean isEmpty (String inputString) {
		if (inputString == null || inputString.trim().equals("")) return true;
		else return false;
	}
	
	public static boolean isNotEmpty (String inputString) {
		if (inputString == null || inputString.trim().equals("")) return false;
		else return true;
	}
	
	
	public static boolean arrayIsEmpty (ArrayList list) {
		if (list == null || list.size() < 1) 	return true;
		else									return false;
	}
	
	public static boolean arrayIsNotEmpty (ArrayList list) {
		if (list != null && list.size() > 0) 	return true;
		else									return false;
	}
	
	
	
	public static String getDetailedExceptionString (Exception exception) {
		StringBuffer detailedError = new StringBuffer();
		
		detailedError.append("Error: "+ exception.getMessage() +"\n");
		for (StackTraceElement element : exception.getStackTrace()) {
			detailedError.append(element.getFileName()+":"+element.getClassName()+":"+element.getMethodName()+":"+element.getLineNumber() +"\n");
		}
		
		return detailedError.toString();
	}
	
	
	
	public static void print (String toPrint) {
		if (toPrint != null) System.out.print(toPrint);
	}
	
	public static void printLine (String toPrint) {
		if (toPrint != null) System.out.println(toPrint);
	}
	
	
	
	private static final String[] trueList = {"true", "yes", "1", "y"};
	
	public static boolean parseBoolean (String input) {
		String upperInput = input.toUpperCase();
		
		for (String value : trueList) {
			if (upperInput.equals(value.toUpperCase())) return true;
		}
		
		return false;
	}
	
	
	public static int parseInt (String input) throws Exception {
		try {
			return Integer.parseInt(input);
		}
		catch (Exception ex) {
			throw new Exception ("Unable to parse integer from value: "+ input);
		}
	}
}
