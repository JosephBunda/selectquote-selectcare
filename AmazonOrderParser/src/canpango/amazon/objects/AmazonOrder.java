package canpango.amazon.objects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import canpango.amazon.objects.AmazonOrder.OrderStatusMessage;
import canpango.utiliy.Utility;

/**	AmazonOrder
 * 	An order placed on Amazon.com. Contains all of the fields in an organized structure.
 * 	@author josephbunda
 */
public class AmazonOrder {
	
	public boolean verbose;

	public InterchangeControlHeader 					interchangeControlHeader;
	public FunctionalGroupHeader						functionalGroupHeader;
	public TransactionSetHeader							transactionSetHeader;
	public BeginningSegmentForPurchaseOrder				beginningSegmentForPurchaseOrder;
	public Currency										currency;
	public ReferenceIdentification_CustomerOrderNumber	referenceIdentification_CustomerOrderNumber;
	public ReferenceIdentification_WebsiteCode			referenceIdentification_WebsiteCode;
	
	public Map <String, OrderStatusMessage>				orderStatusMessages;
	
	public ArrayList <AmazonBillToLocation>				amazonBillToLocations;
	
	public ArrayList <ShipFrom>							vendorWarehouses;
	
	public ArrayList <ShipToPartyDetails>				shipToParties;
	
	public ArrayList <CustomerBillToPartyDetails>		customerBillToParties;
	
	public ArrayList <BaselineItemDataDetails>			baselineItemDataDetails;
	
	public ArrayList <TransactionTotals>				transactionTotalsDetails;
	
	public TransactionSetTrailer						transactionSetTrailer;
	public FunctionalGroupTrailer						functionalGroupTrailer;
	public InterchangeControlTrailer					interchangeControlTrailer;
	
	

	public boolean verifyOrder () {
		boolean hasInterchangeControlHeader 					= (interchangeControlHeader != null);
		boolean hasFunctionalGroupHeader 						= (functionalGroupHeader != null);
		boolean hasTransactionSetHeader 						= (transactionSetHeader != null);
		boolean hasBeginningSegmentForPurchaseOrder 			= (beginningSegmentForPurchaseOrder != null);
		boolean hasCurrency 									= (currency != null);
		boolean hasReferenceIdentification_CustomerOrderNumber 	= (referenceIdentification_CustomerOrderNumber != null);
		
		boolean allMessagesAreCorrect = false;
		boolean messageIncorrect = false;
		Iterator<Entry<String, OrderStatusMessage>> it = orderStatusMessages.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry <String, OrderStatusMessage> pair = it.next();
	        if (pair.getValue().referenceIdentification == null || pair.getValue().messageText == null) {
	        	messageIncorrect = true;
	        	break;
	        }
	    }
	    allMessagesAreCorrect = !messageIncorrect;
	    
	    
	    if (	hasInterchangeControlHeader 
	    	&&	hasFunctionalGroupHeader
	    	&&	hasTransactionSetHeader
	    	&&	hasBeginningSegmentForPurchaseOrder
	    	&&	hasCurrency
	    	&&	hasReferenceIdentification_CustomerOrderNumber
	    	&&	allMessagesAreCorrect) {
	    	return true;
	    }
	    else {
	    	return false;
	    }
	}
	
	
	public AmazonOrder () {
		verbose = false;
		
		interchangeControlHeader 					= null;
		functionalGroupHeader 						= null;
		transactionSetHeader						= null;
		beginningSegmentForPurchaseOrder			= null;
		currency									= null;
		referenceIdentification_CustomerOrderNumber	= null;
		referenceIdentification_WebsiteCode			= null;
		
		orderStatusMessages							= new HashMap <String, OrderStatusMessage> ();
		
		amazonBillToLocations						= new ArrayList <AmazonBillToLocation> ();
		vendorWarehouses							= new ArrayList <ShipFrom> ();
		shipToParties								= new ArrayList <ShipToPartyDetails> ();
		customerBillToParties						= new ArrayList <CustomerBillToPartyDetails> ();
		baselineItemDataDetails						= new ArrayList <BaselineItemDataDetails> ();
		
		transactionTotalsDetails					= new ArrayList <TransactionTotals> ();

		transactionSetTrailer						= null;
		functionalGroupTrailer						= null;
		interchangeControlTrailer					= null;
	}
	
	
	
	public void addInterchangeControlHeader (InterchangeControlHeader interchangeControlHeader) {
		if (this.interchangeControlHeader == null) this.interchangeControlHeader = interchangeControlHeader;
		else if (verbose) Utility.printLine("Interchange Control Header already exists, ignoring new data.");
	}
	
	public void addFunctionalGroupHeader (FunctionalGroupHeader functionalGroupHeader) {
		if (this.functionalGroupHeader == null) this.functionalGroupHeader = functionalGroupHeader;
		else if (verbose) Utility.printLine("Functional Group Header already exists, ignoring new data.");
	}
	
	public void addTransactionSetHeader (TransactionSetHeader transactionSetHeader) {
		if (this.transactionSetHeader == null) this.transactionSetHeader = transactionSetHeader;
		else if (verbose) Utility.printLine("Transaction Set Header already exists, ignoring new data.");
	}
	
	public void addBeginningSegmentForPurchaseOrder (BeginningSegmentForPurchaseOrder beginningSegmentForPurchaseOrder) {
		if (this.beginningSegmentForPurchaseOrder == null) this.beginningSegmentForPurchaseOrder = beginningSegmentForPurchaseOrder;
		else if (verbose) Utility.printLine("Beginning Segment for Purchase Order already exists, ignoring new data.");
	}
	
	public void addCurrency (Currency currency) {
		if (this.currency == null) this.currency = currency;
		else if (verbose) Utility.printLine("Currency already exists, ignoring new data.");
	}
	
	public void addReferenceIdentification_CustomerOrderNumber (ReferenceIdentification_CustomerOrderNumber referenceIdentification_CustomerOrderNumber) {
		if (this.referenceIdentification_CustomerOrderNumber == null) this.referenceIdentification_CustomerOrderNumber = referenceIdentification_CustomerOrderNumber;
		else if (verbose) Utility.printLine("Reference Identification for Customer Order Number already exists, ignoring new data.");
	}
	
	public void addReferenceIdentification_WebsiteCode (ReferenceIdentification_WebsiteCode referenceIdentification_WebsiteCode) {
		if (this.referenceIdentification_WebsiteCode == null) this.referenceIdentification_WebsiteCode = referenceIdentification_WebsiteCode;
		else if (verbose) Utility.printLine("Reference Identification for Webiste Code already exists, ignoring new data.");
	}
	
	
	public class OrderStatusMessage {
		public ReferenceIdentification referenceIdentification;
		public MessageText messageText;
		
		public OrderStatusMessage (ReferenceIdentification referenceIdentification, MessageText messageText) {
			this.referenceIdentification = referenceIdentification;
			this.messageText = messageText;
		}
		
	}
	
	
	
	public void addOrderStatusMessage (ReferenceIdentification referenceIdentification, MessageText messageText) {
		orderStatusMessages.put(referenceIdentification.getKey(), new OrderStatusMessage(referenceIdentification, messageText));
	}
	
	
	public ArrayList <OrderStatusMessage> getOrderStatusMessagesList () {
		ArrayList <OrderStatusMessage> orderStatusMessagesList = new ArrayList <OrderStatusMessage> ();
		Iterator<Entry<String, OrderStatusMessage>> it = orderStatusMessages.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry <String, OrderStatusMessage> pair = it.next();
	        orderStatusMessagesList.add(pair.getValue());
	    }
	    return orderStatusMessagesList;
	}
	
	
	public void addAmazonBillToLocation (AmazonBillToLocation amazonBillToLocation) {
		amazonBillToLocations.add(amazonBillToLocation);
	}
	
	public void addVendorWarehouse (ShipFrom shipFrom) {
		vendorWarehouses.add(shipFrom);
	}
	
	
	public class ShipToPartyDetails {
		public ShipToParty 								shipToParty;
		public ArrayList <AdditionalNameInformation> 	additionalNameInformation;
		public ArrayList <AddressInformation>			addressInformation;
		public GeographicLocation						geographicLocation;
		public ArrayList <CarrierDetails>				carrierDetails;
		
		public ShipToPartyDetails (ShipToParty shipToParty, ArrayList <AdditionalNameInformation> additionalNameInformation, ArrayList <AddressInformation> addressInformation, GeographicLocation geographicLocation, ArrayList <CarrierDetails> carrierDetails) {
			this.shipToParty = shipToParty;
			this.additionalNameInformation = additionalNameInformation;
			this.addressInformation = addressInformation;
			this.geographicLocation = geographicLocation;
			this.carrierDetails = carrierDetails;
		}
		
	}
	
	
	public void addShipToPartyDetails (ShipToParty shipToParty, ArrayList <AdditionalNameInformation> additionalNameInformation, ArrayList <AddressInformation> addressInformation, GeographicLocation geographicLocation, ArrayList <CarrierDetails> carrierDetails) {
		shipToParties.add(new ShipToPartyDetails(shipToParty, additionalNameInformation, addressInformation, geographicLocation, carrierDetails));
	}
	
	
	public class CustomerBillToPartyDetails {
		public CustomerBillToParty 												customerBillToParty;
		public ArrayList <AdditionalNameInformation> 							additionalNameInformation;
		public ArrayList <AddressInformation>									addressInformation;
		public GeographicLocation												geographicLocation;
		public AdministrativeCommunicationsContact_BillToCustomerInformation	administrativeCommunicationsContact_BillToCustomerInformation;
		
		public CustomerBillToPartyDetails (CustomerBillToParty customerBillToParty, ArrayList <AdditionalNameInformation> additionalNameInformation, ArrayList <AddressInformation> addressInformation, GeographicLocation geographicLocation, AdministrativeCommunicationsContact_BillToCustomerInformation administrativeCommunicationsContact_BillToCustomerInformation) {
			this.customerBillToParty = customerBillToParty;
			this.additionalNameInformation = additionalNameInformation;
			this.addressInformation = addressInformation;
			this.geographicLocation = geographicLocation;
			this.administrativeCommunicationsContact_BillToCustomerInformation = administrativeCommunicationsContact_BillToCustomerInformation;
		}
		
	}
	
	
	public void addCustomerBillToPartyDetails (CustomerBillToParty customerBillToParty, ArrayList <AdditionalNameInformation> additionalNameInformation, ArrayList <AddressInformation> addressInformation, GeographicLocation geographicLocation, AdministrativeCommunicationsContact_BillToCustomerInformation administrativeCommunicationsContact_BillToCustomerInformation) {
		customerBillToParties.add(new CustomerBillToPartyDetails(customerBillToParty, additionalNameInformation, addressInformation, geographicLocation, administrativeCommunicationsContact_BillToCustomerInformation));
	}
	
	
	
	
	
	public class BaselineItemDataDetails {
		public BaselineItemData									baselineItemData;
		public ArrayList <PricingInformation_PriceToCustomer>	pricingInformation_PricesToCustomers;
		public MessageText										itemDescription;
		public MessageText										giftMessage;
		
		public BaselineItemDataDetails (BaselineItemData baselineItemData, ArrayList <PricingInformation_PriceToCustomer> pricingInformation_PricesToCustomers, MessageText itemDescription, MessageText giftMessage) {
			this.baselineItemData = baselineItemData;
			this.pricingInformation_PricesToCustomers = pricingInformation_PricesToCustomers;
			this.itemDescription = itemDescription;
			this.giftMessage = giftMessage;
		}
	}
	
	
	public void addBaselineItemDataDetails (BaselineItemData baselineItemData, ArrayList <PricingInformation_PriceToCustomer> pricingInformation_PricesToCustomers, MessageText itemDescription, MessageText giftMessage) {
		baselineItemDataDetails.add(new BaselineItemDataDetails (baselineItemData, pricingInformation_PricesToCustomers, itemDescription, giftMessage));
	}
	
	
	
	public void addTransactionTotals (TransactionTotals transactionTotals) {
		this.transactionTotalsDetails.add(transactionTotals);
	}
	

	public void addTransactionSetTrailer (TransactionSetTrailer transactionSetTrailer) {
		if (this.transactionSetTrailer == null) this.transactionSetTrailer = transactionSetTrailer;
		else if (verbose) Utility.printLine("Transaction Set Trailer already exists, ignoring new data.");
	}
	
	
	public void addFunctionalGroupTrailer (FunctionalGroupTrailer functionalGroupTrailer) {
		if (this.functionalGroupTrailer == null) this.functionalGroupTrailer = functionalGroupTrailer;
		else if (verbose) Utility.printLine("Functional Group Trailer already exists, ignoring new data.");
	}
	
	public void addInterchangeControlTrailer (InterchangeControlTrailer interchangeControlTrailer) {
		if (this.interchangeControlTrailer == null) this.interchangeControlTrailer = interchangeControlTrailer;
		else if (verbose) Utility.printLine("Interchange Control Trailer already exists, ignoring new data.");
	}

	
	public static void parseIntoPurchaseOrderSegment (PurchaseOrderSegment purchaseOrderSegment, String[] inputTokens, boolean verbose) {
		String[] tokenReferenceIDs = purchaseOrderSegment.referenceIDs;
		Map <String, String> segmentValues = purchaseOrderSegment.tokens;
		
		for (int i = 0; i < tokenReferenceIDs.length; i++) {
			try {
				if (i+1 < inputTokens.length) {
					segmentValues.put(tokenReferenceIDs[i], inputTokens[i+1]);	// i+1 is because each line of tokens starts with a header identifier we want to skip.
				}
				else {
					segmentValues.put(tokenReferenceIDs[i], "");
				}
				if (verbose) Utility.printLine("Token '"+tokenReferenceIDs[i]+"' = "+ segmentValues.get(tokenReferenceIDs[i]));
			}
			catch (Exception exception) {
				if (verbose) Utility.printLine("Error obtaining token ["+ tokenReferenceIDs[i] +"], stack trace follows:");
				if (verbose) Utility.printLine(Utility.getDetailedExceptionString(exception));
			}
		}
		
		if (verbose) Utility.printLine("");
	}
	
	
	public class PurchaseOrderSegment {
		public String[] referenceIDs;
		public Map <String, String> tokens;
		
		public PurchaseOrderSegment (String[] referenceIDs) {
			this.referenceIDs = referenceIDs;
			tokens = new HashMap <String, String> ();
		}
		
		
		public String get (String valueName) {
			String value = null;
			value = tokens.get(valueName);
			if (value != null) return value;
			else return "";
		}
		
		public String getSQLValue (String valueName) {
			return getSQLValue(valueName, true);
		}
		
		public String getSQLValue (String valueName, boolean addComma) {
			return "\'"+ get(valueName).replace("'", "\\'") +"\'"+ (addComma ? ", " : "");
		}
		
	}
	
	/*
	public class PurchaseOrderSegmentMetadata {
		public String 	reference;
		public String 	id;
		public String 	name;
		public int 		minimumCharacters;
		public int 		maximumCharacters;
	}
	*/
	

	
	
	public static final String[] Segment_InterchangeControlHeader = {
		"ISA01",		// Authorization Information Qualifier
		"ISA02",		// Authorization Information	
		"ISA03",		// Security Information Qualifier
		"ISA04",		// Security Information
		"ISA05",		// Interchange ID Qualifier (1)
		"ISA06",		// Interchange Sender ID
		"ISA07",		// Interchange ID Qualifier (2)
		"ISA08",		// Interchange Receiver ID
		"ISA09",		// Interchange Date
		"ISA10",		// Interchange Time
		"ISA11",		// Interchange Control Standards Indentifier
		"ISA12",		// Interchange Control Version Number
		"ISA13",		// Interchange Control Number
		"ISA14",		// Acknowledgment Requested
		"ISA15",		// Usage Indicator
		"ISA16"			// Component Element Seperator
	};
	public class InterchangeControlHeader extends PurchaseOrderSegment {
		
		public InterchangeControlHeader () {
			super(Segment_InterchangeControlHeader);
		}
	}
	
	
	
	
	
	public static final String[] Segment_FunctionalGroupHeader = {
		"GS01",		// Functional Identifier Code
		"GS02",		// Application Sender's Code	
		"GS03",		// Application Receiver's Code
		"GS04",		// Date
		"GS05",		// Time
		"GS06",		// Group Control Number
		"GS07",		// Responsible Agency Code
		"GS08",		// Version / Release / Industry Identifier Code
	};
	public class FunctionalGroupHeader extends PurchaseOrderSegment {
		
		public FunctionalGroupHeader () {
			super(Segment_FunctionalGroupHeader);
		}
	}
	
	
	
	
	public static final String[] Segment_TransactionSetHeader = {
		"ST01",		// Transaction Set Identifier Code
		"ST02",		// Transaction Set Control Number
	};
	public class TransactionSetHeader extends PurchaseOrderSegment{
		
		public TransactionSetHeader () {
			super(Segment_TransactionSetHeader);
		}
	}
	
	
	
	
	public static final String[] Segment_BeginningSegmentForPurchaseOrder = {
		"BEG01",		// Transaction Set Purpose Code
		"BEG02",		// Purchase Order Type Code
		"BEG03",		// Purchase Order Number
		"BEG04",		// (Not used)	
		"BEG05"			// Date
	};
	public class BeginningSegmentForPurchaseOrder extends PurchaseOrderSegment {
		
		public BeginningSegmentForPurchaseOrder () {
			super(Segment_BeginningSegmentForPurchaseOrder);
		}
	}
	
	
	
	
	public static final String[] Segment_Currency = {
		"CUR01",		// Entity Identifier Code
		"CUR02"			// Currency Code
	};
	public class Currency extends PurchaseOrderSegment {
		
		public Currency () {
			super(Segment_Currency);
		}
	}
	
	
	
	
	public static final String[] Segment_ReferenceIdentification_CustomerOrderNumber = {
		"REF01",		// Reference Identification Qualifier
		"REF02"			// Reference Identification - Customer Order Number
	};
	public class ReferenceIdentification_CustomerOrderNumber extends PurchaseOrderSegment {
		
		public ReferenceIdentification_CustomerOrderNumber () {
			super(Segment_ReferenceIdentification_CustomerOrderNumber);
		}
	}
	
	
	
	
	public static final String[] Segment_ReferenceIdentification_WebsiteCode = {
		"REF01",		// Reference Identification Qualifier
		"REF02"			// Reference Identification
	};
	public class ReferenceIdentification_WebsiteCode extends PurchaseOrderSegment {
		
		public ReferenceIdentification_WebsiteCode () {
			super(Segment_ReferenceIdentification_WebsiteCode);
		}
	}
	
	
	
	public static final String[] Segment_LoopReferenceIdentification_OrderStatusMessage = {
		"295",			// Reference Identification
		"300"			// Message Text
	};
	public class LoopReferenceIdentification_OrderStatusMessage extends PurchaseOrderSegment {
		
		public LoopReferenceIdentification_OrderStatusMessage () {
			super(Segment_LoopReferenceIdentification_OrderStatusMessage);
		}
	}
	
	
	
	
	public static final String[] Segment_ReferenceIdentification = {
		"N901",			// Reference Identification Qualifier
		"N902"			// Reference Identification
	};
	public class ReferenceIdentification extends PurchaseOrderSegment {
		
		public ReferenceIdentification () {
			super(Segment_ReferenceIdentification);
		}
		
		public String getKey () {
			return tokens.get("N902");
		}
	}
	
	
	
	
	public static final String[] Segment_MessageText = {
			"MSG01"			// Free-Form Message Text
	};
	public class MessageText extends PurchaseOrderSegment {
		
		public MessageText () {
			super(Segment_MessageText);
		}
	}	
	
	
	
	
	public static final String[] Segment_AmazonBillToLocation = {
		"N101",			// Entity Identifier Code (Always BT)
		"N102",			// Name
		"N103",			// Identification Code Qualifier
		"N104"			// Identification Code
	};
	public class AmazonBillToLocation extends PurchaseOrderSegment {
		
		public AmazonBillToLocation () {
			super(Segment_AmazonBillToLocation);
		}
	}
	
	
	
	
	public static final String[] Segment_CustomerBillToParty = {
		"N101",			// Entity Identifier Code (Always LW)
		"N102",			// Name
	};
	public class CustomerBillToParty extends PurchaseOrderSegment {
		
		public CustomerBillToParty () {
			super(Segment_CustomerBillToParty);
		}
	}	
	
	
	
	public static final String[] Segment_ShipFrom = {
		"N101",			// Entity Identifier Code
		"N102",			// Name - Ship From
		"N103",			// Identification Code Qualifier
		"N104"			// Identification Code
	};
	public class ShipFrom extends PurchaseOrderSegment {
		
		public ShipFrom () {
			super(Segment_ShipFrom);
		}
	}	
	
	
	
	public static final String[] Segment_ShipToParty = {
		"CTT01",			// Number of Line Items
		"CTT02",			// Hash Total
	};
	public class ShipToParty extends PurchaseOrderSegment {
		
		public ShipToParty () {
			super(Segment_ShipToParty);
		}
	}
	
	
	
	public static final String[] Segment_AdditionalNameInformation = {
		"N201",			// Name
		"N202",			// Name
	};
	public class AdditionalNameInformation extends PurchaseOrderSegment {
		
		public AdditionalNameInformation () {
			super(Segment_AdditionalNameInformation);
		}
	}
	
	
	
	public static final String[] Segment_AddressInformation = {
		"N301",			// Address Information Line 1
		"N302",			// Address Information Line 2
	};
	public class AddressInformation extends PurchaseOrderSegment {
		
		public AddressInformation () {
			super(Segment_AddressInformation);
		}
	}
	
	
	public static final String[] Segment_GeographicLocation = {
		"N401",			// City Name
		"N402",			// State or Province Code
		"N403",			// Postal Code
		"N404",			// Country Code
		"N405",			// Location Qualifier
		"N406",			// Location Identifier
	};
	public class GeographicLocation extends PurchaseOrderSegment {
		
		public GeographicLocation () {
			super(Segment_GeographicLocation);
		}
	}
	
	
	
	public static final String[] Segment_CarrierDetails = {
		"TD501",			// (Not used)
		"TD502",			// Identification Code Qualifier
		"TD503",			// Identification Code
		"TD504",			// (Not used)
		"TD505",			// (Not used)
		"TD506",			// (Not used)
		"TD507",			// Location Qualifier
		"TD508"				// Location Identifier
	};
	public class CarrierDetails extends PurchaseOrderSegment {
		
		public CarrierDetails () {
			super(Segment_CarrierDetails);
		}
	}
	
	
	public static final String[] Segment_AdministrativeCommunicationsContact_BillToCustomerInformation = {
		"PER01",			// Contact Function Code (Always ZZ)
		"PER02",			// Name
		"PER03",			// Communcation Number Qualifier - Email
		"PER04",			// Communication Number
		"PER05",			// Communication Number Qualifier - Telephone
		"PER06"				// Communication Number
	};
	public class AdministrativeCommunicationsContact_BillToCustomerInformation extends PurchaseOrderSegment {
		
		public AdministrativeCommunicationsContact_BillToCustomerInformation () {
			super(Segment_AdministrativeCommunicationsContact_BillToCustomerInformation);
		}
	}

	
	public static final String[] Segment_BaselineItemData = {
		"PO101",			// Assigned Identification
		"PO102",			// Quantity Ordered
		"PO103",			// Unit or Basis for Measurement Code
		"PO104",			// Unit Price
		"PO105",			// Basis of Unit Price Code
		"PO106",			// Product/Service ID Qualifier
		"PO107",			// Product/Service ID
		"PO108",			// Product/Service ID Qualifier
		"PO109",			// Product/Service ID
		"PO110",			// Product/Service ID Qualifier
		"PO111",			// Product/Service ID
		"PO112",			// Product/Service ID Qualifier
		"PO113",			// Product/Service ID
		"PO114",			// Product/Service ID Qualifier
		"PO115"				// Product/Service ID
	};
	public class BaselineItemData extends PurchaseOrderSegment {
		
		public BaselineItemData () {
			super(Segment_BaselineItemData);
		}
	}
	
	
	public static final String[] Segment_PricingInformation_PriceToCustomer = {
		"CTP01",			// (Not Used)
		"CTP02",			// Price Identifier Code
		"CTP03"				// Unit Price
	};
	public class PricingInformation_PriceToCustomer extends PurchaseOrderSegment {
		
		public PricingInformation_PriceToCustomer () {
			super(Segment_PricingInformation_PriceToCustomer);
		}
	}
	
	
	public static final String[] Segment_TransactionTotals = {
		"CTT01",			// Number of Line Items
		"CTT02",			// Hash Total
	};
	public class TransactionTotals extends PurchaseOrderSegment {
		
		public TransactionTotals () {
			super(Segment_TransactionTotals);
		}
	}
	
	
	
	
	public static final String[] Segment_TransactionSetTrailer = {
		"SE01",			// Number of Included Segments
		"SE02",			// Transaction Set Control Number
	};
	public class TransactionSetTrailer extends PurchaseOrderSegment {
		
		public TransactionSetTrailer () {
			super(Segment_TransactionSetTrailer);
		}
	}
	
	
	
	
	public static final String[] Segment_FunctionalGroupTrailer = {
		"GE01",			// Number of Transaction Set Included
		"GE02",			// Group Control Number
	};
	public class FunctionalGroupTrailer extends PurchaseOrderSegment {
		
		public FunctionalGroupTrailer () {
			super(Segment_FunctionalGroupTrailer);
		}
	}
	
	
	
	
	public static final String[] Segment_InterchangeControlTrailer = {
		"IEA01",			// Number of Included Functional Groups
		"IEA02",			// Interchange Control Number
	};
	public class InterchangeControlTrailer extends PurchaseOrderSegment {
		
		public InterchangeControlTrailer () {
			super(Segment_InterchangeControlTrailer);
		}
	}
}
