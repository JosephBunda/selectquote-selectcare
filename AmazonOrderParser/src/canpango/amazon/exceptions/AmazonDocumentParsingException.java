package canpango.amazon.exceptions;



public class AmazonDocumentParsingException extends Exception {

	private static final long serialVersionUID = 440434818356064948L;
	
	public AmazonDocumentParsingException (String message) {
		super(message);
	}
}
