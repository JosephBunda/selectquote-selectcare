package canpango.amazon.process;

import canpango.amazon.objects.AmazonOrder;
import canpango.amazon.objects.AmazonOrder.AdministrativeCommunicationsContact_BillToCustomerInformation;
import canpango.utiliy.Utility;

import java.util.ArrayList;
import java.util.HashMap;

import canpango.amazon.exceptions.AmazonDocumentParsingException;



/**	OrderParser
 * 	Receives a string that represents an order from Amazon.com and outputs an AmazonOrder object.
 * 	The parser requires the type of order that is being parsed.
 */
public class OrderParser {

	/** parseOrder
	 * 	Translates the input string according to the type of order indicated.
	 * 	@param	documentType	The type of order
	 * 	@param	orderContent	The string content of the order
	 */
	public static AmazonOrder parse (String documentType, String documentContent, boolean verbose) throws AmazonDocumentParsingException {
		
		AmazonOrder amazonOrder = null;
		
		// Do a smoke check on the document content.
		if (documentContent == null || documentContent.length() == 0) {
			throw new AmazonDocumentParsingException("The input document content is empty!");
		}
		
		// Select the appropriate document type for parsing.
		if (documentType.equals("850")) {
			amazonOrder = new AmazonOrder();
			parse850Document(amazonOrder, documentContent, verbose);
		}
		else {
			throw new AmazonDocumentParsingException("The specified document type is not valid. Valid types include: 850");
		}
		
		return amazonOrder;
	}
	
	
	// These are variables that will always be equal to the first and second tokens in a newly-read line.
	private static String token0;
	private static String token1;
	
	private static void parse850Document (AmazonOrder amazonDocument, String documentContent, boolean verbose) {
		String lineDelimiter = "~";
		
		
		String ISA 	= "ISA";		// Interchange Control Header
		String GS 	= "GS";			// Functional Group Header
		String ST	= "ST";			// Transaction Set Header
		String BEG	= "BEG";		// Beginning Segment for Purchase Order
		String CUR	= "CUR";		// Currency
		String REF	= "REF";		// Reference Identification
			String REF_OQ = "OQ";			// Customer Order Number 
			String REF_ST = "ST";			// Website Code 
			
		String N9	= "N9";			// Reference Identification (Message)
		String MSG	= "MSG";		// Message
		
		String N1	= "N1";
			String N1_BT	= "BT";		// Amazon Bill To Location
			String N1_SF 	= "SF"; 	// Ship From Vendor Warehouse
			String N1_ST	= "ST";		// Ship To Party
		
		String N2	= "N2";		// Additional Name Information
		String N3	= "N3";		// Address Information
		String N4	= "N4";		// Geographic Location
		String TD5	= "TD5";	// Carrier Details
		
		String N1_LW	= "LW";	// Customer Bill To Party
		
		String PER	= "PER";	// Administrative Communications Contact - Bill To Customer Information
		
		String PO1	= "PO1";	// Baseline Item Data
		String CTP	= "CTP";	// Price to Customer
		
		String CTT	= "CTT";	// Transaction Totals
		String SE	= "SE";		// Transaction Set Trailer
		String GE	= "GE";		// Functional Group Trailer
		String IEA	= "IEA";	// Interchange Control Trailer
		
		
		String[] lines = documentContent.split(lineDelimiter);
		
		for (int i = 0; i < lines.length; i++) {
			
			String[] tokens = getLineTokens(lines[i], verbose);
			
			
			
			if (token0.equals(ISA)) {
				AmazonOrder.InterchangeControlHeader segment = amazonDocument.new InterchangeControlHeader();
				AmazonOrder.parseIntoPurchaseOrderSegment(segment, tokens, verbose);
				amazonDocument.addInterchangeControlHeader(segment);
			}
			else if (token0.equals(GS)) {
				AmazonOrder.FunctionalGroupHeader segment = amazonDocument.new FunctionalGroupHeader();
				AmazonOrder.parseIntoPurchaseOrderSegment(segment, tokens, verbose);
				amazonDocument.addFunctionalGroupHeader(segment);
			}
			else if (token0.equals(ST)) {
				AmazonOrder.TransactionSetHeader segment = amazonDocument.new TransactionSetHeader();
				AmazonOrder.parseIntoPurchaseOrderSegment(segment, tokens, verbose);
				amazonDocument.addTransactionSetHeader(segment);
			}
			else if (token0.equals(BEG)) {
				AmazonOrder.BeginningSegmentForPurchaseOrder segment = amazonDocument.new BeginningSegmentForPurchaseOrder();
				AmazonOrder.parseIntoPurchaseOrderSegment(segment, tokens, verbose);
				amazonDocument.addBeginningSegmentForPurchaseOrder(segment);
			}
			else if (token0.equals(CUR)) {
				AmazonOrder.Currency segment = amazonDocument.new Currency();
				AmazonOrder.parseIntoPurchaseOrderSegment(segment, tokens, verbose);
				amazonDocument.addCurrency(segment);
			}
			else if (token0.equals(REF) && token1.equals(REF_OQ)) {
				AmazonOrder.ReferenceIdentification_CustomerOrderNumber segment = amazonDocument.new ReferenceIdentification_CustomerOrderNumber();
				AmazonOrder.parseIntoPurchaseOrderSegment(segment, tokens, verbose);
				amazonDocument.addReferenceIdentification_CustomerOrderNumber(segment);
			}
			else if (token0.equals(REF) && token1.equals(REF_ST)) {
				AmazonOrder.ReferenceIdentification_WebsiteCode segment = amazonDocument.new ReferenceIdentification_WebsiteCode();
				AmazonOrder.parseIntoPurchaseOrderSegment(segment, tokens, verbose);
				amazonDocument.addReferenceIdentification_WebsiteCode(segment);
			}
			else if (token0.equals(N9)) {
				AmazonOrder.ReferenceIdentification referenceIdentificationSegment = amazonDocument.new ReferenceIdentification();
				AmazonOrder.parseIntoPurchaseOrderSegment(referenceIdentificationSegment, tokens, verbose);
				if (++i < lines.length) {
					tokens = getLineTokens(lines[i], verbose);
					if (token0.equals(MSG)) {
						AmazonOrder.MessageText messsgeTextSegment = amazonDocument.new MessageText();
						AmazonOrder.parseIntoPurchaseOrderSegment(messsgeTextSegment, tokens, verbose);
						amazonDocument.addOrderStatusMessage(referenceIdentificationSegment, messsgeTextSegment);
					}
				}
				
			}
			else if (token0.equals(N1) && token1.equals(N1_BT)) {
				AmazonOrder.AmazonBillToLocation billToLocationSegment = amazonDocument.new AmazonBillToLocation();
				AmazonOrder.parseIntoPurchaseOrderSegment(billToLocationSegment, tokens, verbose);
				amazonDocument.addAmazonBillToLocation(billToLocationSegment);
			}
			else if (token0.equals(N1) && token1.equals(N1_SF)) { 
				AmazonOrder.ShipFrom shipFromSegment = amazonDocument.new ShipFrom();
				AmazonOrder.parseIntoPurchaseOrderSegment(shipFromSegment, tokens, verbose);
				amazonDocument.addVendorWarehouse(shipFromSegment);
			}
			else if (token0.equals(N1) && token1.equals(N1_ST)) {
				// Create the placeholders for this loop.
				AmazonOrder.ShipToParty shipToPartySegment = amazonDocument.new ShipToParty();
				ArrayList<AmazonOrder.AdditionalNameInformation> additionalNameInformationSegments = new ArrayList<AmazonOrder.AdditionalNameInformation>();
				ArrayList<AmazonOrder.AddressInformation> addressInformationSegments = new ArrayList<AmazonOrder.AddressInformation>();
				AmazonOrder.GeographicLocation geographicLocationSegment = amazonDocument.new GeographicLocation();
				ArrayList<AmazonOrder.CarrierDetails> carrierDetailsSegments = new ArrayList <AmazonOrder.CarrierDetails>();
				
				String lastToken0 = token0;
				do {
					if (token0.equals(N1) && token1.equals(N1_ST)) {
						AmazonOrder.parseIntoPurchaseOrderSegment(shipToPartySegment, tokens, verbose);
					}
					else if (token0.equals(N2)) {
						AmazonOrder.AdditionalNameInformation additionalNameInformationSegment = amazonDocument.new AdditionalNameInformation();
						AmazonOrder.parseIntoPurchaseOrderSegment(additionalNameInformationSegment, tokens, verbose);
						additionalNameInformationSegments.add(additionalNameInformationSegment);
					}
					else if (token0.equals(N3)) {
						AmazonOrder.AddressInformation addressInformationSegment = amazonDocument.new AddressInformation();
						AmazonOrder.parseIntoPurchaseOrderSegment(addressInformationSegment, tokens, verbose);
						addressInformationSegments.add(addressInformationSegment);
					}
					else if (token0.equals(N4)) {
						AmazonOrder.parseIntoPurchaseOrderSegment(geographicLocationSegment, tokens, verbose);
					}
					else if (token0.equals(TD5)) {
						AmazonOrder.CarrierDetails carrierDetailsSegment = amazonDocument.new CarrierDetails();
						AmazonOrder.parseIntoPurchaseOrderSegment(carrierDetailsSegment, tokens, verbose);
						carrierDetailsSegments.add(carrierDetailsSegment);
					}
					
					lastToken0 = token0;
					tokens = getLineTokens(lines[++i], verbose);
				} while (!(lastToken0.equals(TD5) && (token0.equals(N1) && token1.equals(N1_LW))) && i < lines.length);
				
				amazonDocument.addShipToPartyDetails(shipToPartySegment, additionalNameInformationSegments, addressInformationSegments, geographicLocationSegment, carrierDetailsSegments);
				
				// Back up one line because we found the exit condition for this loop.
				i--;
			}
			
			else if (token0.equals(N1) && token1.equals(N1_LW)) {
				// Create the placeholders for this loop.
				AmazonOrder.CustomerBillToParty customerBillToPartySegment = amazonDocument.new CustomerBillToParty();
				ArrayList<AmazonOrder.AdditionalNameInformation> additionalNameInformationSegments = new ArrayList<AmazonOrder.AdditionalNameInformation>();
				ArrayList<AmazonOrder.AddressInformation> addressInformationSegments = new ArrayList<AmazonOrder.AddressInformation>();
				AmazonOrder.GeographicLocation geographicLocationSegment = amazonDocument.new GeographicLocation();
				AmazonOrder.AdministrativeCommunicationsContact_BillToCustomerInformation administrativeCommunicationsContact_BillToCustomerInformationSegment = amazonDocument.new AdministrativeCommunicationsContact_BillToCustomerInformation();
				
				String lastToken0 = token0;
				do {
					if (token0.equals(N1) && token1.equals(N1_LW)) {
						AmazonOrder.parseIntoPurchaseOrderSegment(customerBillToPartySegment, tokens, verbose);
					}
					else if (token0.equals(N2)) {
						AmazonOrder.AdditionalNameInformation additionalNameInformationSegment = amazonDocument.new AdditionalNameInformation();
						AmazonOrder.parseIntoPurchaseOrderSegment(additionalNameInformationSegment, tokens, verbose);
						additionalNameInformationSegments.add(additionalNameInformationSegment);
					}
					else if (token0.equals(N3)) {
						AmazonOrder.AddressInformation addressInformationSegment = amazonDocument.new AddressInformation();
						AmazonOrder.parseIntoPurchaseOrderSegment(addressInformationSegment, tokens, verbose);
						addressInformationSegments.add(addressInformationSegment);
					}
					else if (token0.equals(N4)) {
						AmazonOrder.parseIntoPurchaseOrderSegment(geographicLocationSegment, tokens, verbose);
					}
					else if (token0.equals(PER)) {
						AmazonOrder.parseIntoPurchaseOrderSegment(administrativeCommunicationsContact_BillToCustomerInformationSegment, tokens, verbose);
					}
					
					lastToken0 = token0;
					tokens = getLineTokens(lines[++i], verbose);
				} while (!token0.equals(PO1) && i < lines.length);
				
				amazonDocument.addCustomerBillToPartyDetails(customerBillToPartySegment, additionalNameInformationSegments, addressInformationSegments, geographicLocationSegment, administrativeCommunicationsContact_BillToCustomerInformationSegment);
				
				// Back up one line because we found the exit condition for this loop.
				i--;
			}
			
			else if (token0.equals(PO1)) {
				// Create the placeholders for this loop.
				AmazonOrder.BaselineItemData baselineItemDataSegment = amazonDocument.new BaselineItemData();
				ArrayList <AmazonOrder.PricingInformation_PriceToCustomer> pricingInformation_PriceToCustomerSegments = new ArrayList<AmazonOrder.PricingInformation_PriceToCustomer>();
				AmazonOrder.MessageText itemDescriptionSegment = amazonDocument.new MessageText();
				AmazonOrder.MessageText giftMessageSegment = amazonDocument.new MessageText();
				
				String lastToken0 = token0;
				do {
					if (token0.equals(PO1)) {
						AmazonOrder.parseIntoPurchaseOrderSegment(baselineItemDataSegment, tokens, verbose);
					}
					else if (token0.equals(CTP)) {
						AmazonOrder.PricingInformation_PriceToCustomer pricingInformation_PriceToCustomerSegment = amazonDocument.new PricingInformation_PriceToCustomer();
						AmazonOrder.parseIntoPurchaseOrderSegment(pricingInformation_PriceToCustomerSegment, tokens, verbose);
						pricingInformation_PriceToCustomerSegments.add(pricingInformation_PriceToCustomerSegment);
					}
					else if (!lastToken0.equals(MSG) && token0.equals(MSG)) {
						AmazonOrder.parseIntoPurchaseOrderSegment(itemDescriptionSegment, tokens, verbose);
					}
					else if (lastToken0.equals(MSG) && token0.equals(MSG)) {
						AmazonOrder.parseIntoPurchaseOrderSegment(giftMessageSegment, tokens, verbose);
					}
					
					lastToken0 = token0;
					tokens = getLineTokens(lines[++i], verbose);
				} while (!(!lastToken0.equals(PO1) && token0.equals(PO1)) && !token0.equals(CTT) && i < lines.length);
				
			
				amazonDocument.addBaselineItemDataDetails(baselineItemDataSegment, pricingInformation_PriceToCustomerSegments, itemDescriptionSegment, giftMessageSegment);
				
				// Back up one line because we found the exit condition for this loop.
				i--;
			}

			
			else if (token0.equals(CTT)) {
				AmazonOrder.TransactionTotals segment = amazonDocument.new TransactionTotals();
				AmazonOrder.parseIntoPurchaseOrderSegment(segment, tokens, verbose);
				amazonDocument.addTransactionTotals(segment);
			}
			
			else if (token0.equals(SE)) {
				AmazonOrder.TransactionSetTrailer segment = amazonDocument.new TransactionSetTrailer();
				AmazonOrder.parseIntoPurchaseOrderSegment(segment, tokens, verbose);
				amazonDocument.addTransactionSetTrailer(segment);
			}
			
			else if (token0.equals(GE)) {
				AmazonOrder.FunctionalGroupTrailer segment = amazonDocument.new FunctionalGroupTrailer();
				AmazonOrder.parseIntoPurchaseOrderSegment(segment, tokens, verbose);
				amazonDocument.addFunctionalGroupTrailer(segment);
			}
			
			else if (token0.equals(IEA)) {
				AmazonOrder.InterchangeControlTrailer segment = amazonDocument.new InterchangeControlTrailer();
				AmazonOrder.parseIntoPurchaseOrderSegment(segment, tokens, verbose);
				amazonDocument.addInterchangeControlTrailer(segment);
			}
		}
	}
	
	
	private static String[] getLineTokens (String line, boolean verbose) {
		String tokenDelimiter = "\\^";
		// Replace line breaks with spaces.
		line = line.replace("\n", " ");
		
		if (verbose) Utility.printLine("Line: "+ line);
		
		String[] tokens;
		
		if (Utility.isNotEmpty(line)) {
			tokens = line.split(tokenDelimiter);
			
			// The first token in a line may have a leading space.
			tokens[0] = tokens[0].trim();
			
			token0 = tokens[0];
			token1 = tokens[1];
		}
		else {
			tokens = null;
			token0 = "";
			token1 = "";
		}
		
		return tokens;
	}
	
}
