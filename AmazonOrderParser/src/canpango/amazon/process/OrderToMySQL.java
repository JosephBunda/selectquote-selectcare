package canpango.amazon.process;

import java.util.ArrayList;

import canpango.amazon.objects.AmazonOrder;
import canpango.amazon.objects.AmazonOrder.PricingInformation_PriceToCustomer;
import canpango.utiliy.Utility;

public class OrderToMySQL {

	public class OrderInsertBatch {
		public String insertOrderHeaderStatement;
		public String insertOrderMessagesStatement;
		public String insertOrderLinesStatement;
		
		public OrderInsertBatch () {
			insertOrderHeaderStatement = "";
			insertOrderMessagesStatement = "";
			insertOrderLinesStatement = "";
		}
		
		public String[] getStatements () {
			String[] statements = new String[3];
			statements[0] = insertOrderHeaderStatement;
			statements[1] = insertOrderMessagesStatement;
			statements[2] = insertOrderLinesStatement;
			return statements;
		}
	}
	
	
	public static OrderInsertBatch createOrderInsertBatch (AmazonOrder amazonOrder, boolean verbose) {
		if (verbose) Utility.printLine("");
		if (verbose) Utility.printLine("Constructing SQL to insert order...");
		
		OrderInsertBatch batch = new OrderToMySQL().new OrderInsertBatch();
		
		String orderID = amazonOrder.beginningSegmentForPurchaseOrder.get("BEG03");
		if (verbose) Utility.printLine("Order ID is: "+ orderID);
		
		batch.insertOrderHeaderStatement = getOrderHeaderInsertSQL(amazonOrder);
		if (verbose) Utility.printLine("Insert Header Statement: "+ batch.insertOrderHeaderStatement);
		
		batch.insertOrderMessagesStatement = getOrderMessagesInsertSQL(amazonOrder, orderID);
		if (verbose) Utility.printLine("Insert Messages Statement: "+ batch.insertOrderMessagesStatement);
		
		batch.insertOrderLinesStatement = getOrderLinesInsertSQL(amazonOrder, orderID);
		if (verbose) Utility.printLine("Insert Lines Statement: "+ batch.insertOrderLinesStatement);
		
		if (verbose) Utility.printLine("SQL insert statement batch created.");
		if (verbose) Utility.printLine("");
		
		return batch;
	}
	
	
public static String getOrderLinesInsertSQL (AmazonOrder amazonOrder, String orderID) {
		
		ArrayList <AmazonOrder.BaselineItemDataDetails> baselineItemDetails = amazonOrder.baselineItemDataDetails;
		
		StringBuffer q = new StringBuffer();
		
		q.append("insert into 850_OFR_LBID (");
		
			q.append("BEG03, ");
			q.append("PO101, PO102, PO103, PO104, PO105, PO106, PO107, PO108, PO109, PO110, PO111, PO112, PO113, PO114, PO115, ");
			q.append("CTP02, CTP03, ");
			q.append("MSG01ID, MSG01GM, ");
			q.append("LINE_UNIQUE");
			
		q.append(") values ");
		
			int lineIndex = 1;
			
			StringBuffer values = new StringBuffer();
			for (AmazonOrder.BaselineItemDataDetails lineItem : baselineItemDetails) {
				if (!values.toString().equals("")) values.append(", ");
				
				values.append("(");
				
				values.append("\'"+ orderID +"\', ");
				
				AmazonOrder.BaselineItemData item = lineItem.baselineItemData;
				values.append(item.getSQLValue("PO101"));
				values.append(getIntFromString(item.get("PO102"))+", ");
				values.append(item.getSQLValue("PO103"));
				values.append(getDecimalFromString(item.get("PO104"))+", ");
				values.append(item.getSQLValue("PO105"));
				values.append(item.getSQLValue("PO106"));
				values.append(item.getSQLValue("PO107"));
				values.append(item.getSQLValue("PO108"));
				values.append(item.getSQLValue("PO109"));
				values.append(item.getSQLValue("PO110"));
				values.append(item.getSQLValue("PO111"));
				values.append(item.getSQLValue("PO112"));
				values.append(item.getSQLValue("PO113"));
				values.append(item.getSQLValue("PO114"));
				values.append(item.getSQLValue("PO115"));
				
				if (Utility.arrayIsNotEmpty(lineItem.pricingInformation_PricesToCustomers)) {
					PricingInformation_PriceToCustomer pricesToCustomers = lineItem.pricingInformation_PricesToCustomers.get(0);
					values.append(pricesToCustomers.getSQLValue("CTP02"));
					values.append(getDecimalFromString(pricesToCustomers.get("CTP03")));
				}
				else {
					values.append("null, ");
					values.append("null");
				}
				
				if (lineItem.itemDescription != null) {
					AmazonOrder.MessageText msg = lineItem.itemDescription;
					values.append(", "+ msg.getSQLValue("MSG01", false));
				}
				else {
					values.append(", null");
				}
				
				if (lineItem.giftMessage != null) {
					AmazonOrder.MessageText msg = lineItem.giftMessage;
					values.append(", "+ msg.getSQLValue("MSG01", false));
				}
				else {
					values.append(", null");
				}
				
				values.append(", \'"+ orderID +"_"+ lineIndex +"\'");
				
				values.append(")");
				
				lineIndex++;
			}
			
			q.append(values.toString());
		
		String finishedSQL = q.toString();
		
		return finishedSQL;
	}
	
	public static String getOrderMessagesInsertSQL (AmazonOrder amazonOrder, String orderID) {
		
		ArrayList <AmazonOrder.OrderStatusMessage> orderStatusMessages = amazonOrder.getOrderStatusMessagesList();
		
		StringBuffer q = new StringBuffer();
		
		q.append("insert into 850_OFR_N9MSG (");
		
			q.append("BEG03, N901, N902, MSG01");
			
		q.append(") values ");
		
			StringBuffer values = new StringBuffer();
			for (AmazonOrder.OrderStatusMessage message : orderStatusMessages) {
				if (!values.toString().equals("")) values.append(", ");
				
				values.append("(");
				
				values.append("\'"+ orderID +"\', ");
				
				AmazonOrder.ReferenceIdentification ref = message.referenceIdentification;
				values.append(ref.getSQLValue("N901"));
				values.append(ref.getSQLValue("N902"));
				
				AmazonOrder.MessageText msg = message.messageText;
				values.append(msg.getSQLValue("MSG01", false));
				values.append(")");
			}
			
			q.append(values.toString());
		
		String finishedSQL = q.toString();
		
		return finishedSQL;
	}
	
	
	
	public static String getOrderHeaderInsertSQL (AmazonOrder amazonOrder) {
		AmazonOrder.ShipToPartyDetails shipToPartyDetails = amazonOrder.shipToParties.get(0);
		AmazonOrder.CustomerBillToPartyDetails customerBillToPartyDetails = amazonOrder.customerBillToParties.get(0);
		
		
		StringBuffer q = new StringBuffer();
		
		q.append("insert into 850_OFR (");
		
			q.append("ISA01, ISA02, ISA03, ISA04, ISA05, ISA06, ISA07, ISA08, ISA09, ISA10, ISA11, ISA12, ISA13, ISA14, ISA15, ");
			q.append("GS01, GS02, GS03, GS04, GS05, GS06, GS07, GS08, ");
			q.append("ST01, ST02, ");
			q.append("BEG01, BEG02, BEG03, BEG05, ");
			q.append("CUR01, CUR02, ");
			q.append("REF01OQ, REF02OQ, ");
			
			if (amazonOrder.referenceIdentification_WebsiteCode != null) {
				q.append("REF01ST, REF02ST, ");
			}
			
			q.append("N101BT, N102BT, N103BT, N104BT, ");
			q.append("N101SF, N102SF, N103SF, N104SF, ");
			q.append("N101ST, N102ST, ");
			
			if (Utility.arrayIsNotEmpty(shipToPartyDetails.additionalNameInformation)) {
				q.append("N201ST, N202ST, ");
			}
			
			if (Utility.arrayIsNotEmpty(shipToPartyDetails.addressInformation)) {
				q.append("N301ST, N302ST, ");
			}
			
			q.append("N401ST, N402ST, N403ST, N404ST, N405ST, N406ST, ");
			q.append("TD502, TD503, TD507, TD508, ");
			q.append("N101LW, N102LW, ");
			
			if (Utility.arrayIsNotEmpty(customerBillToPartyDetails.additionalNameInformation)) {
				q.append("N201LW, N202LW, ");
			}
			
			if (Utility.arrayIsNotEmpty(customerBillToPartyDetails.addressInformation)) {
				q.append("N301LW, N302LW, ");
			}
			
			q.append("N401LW, N402LW, N403LW, N404LW, ");
			
			if (customerBillToPartyDetails.administrativeCommunicationsContact_BillToCustomerInformation != null) {
				q.append("PER01, PER02, PER03, PER04, PER05, PER06, ");
			}
			
			q.append("CTT01, CTT02, ");
			q.append("SE01, SE02, ");
			q.append("GE01, GE02, ");
			q.append("IEA01, IEA02");
			
		q.append(") values (");
		
			AmazonOrder.InterchangeControlHeader isa = amazonOrder.interchangeControlHeader;
			q.append(isa.getSQLValue("ISA01"));
			q.append(isa.getSQLValue("ISA02"));
			q.append(isa.getSQLValue("ISA03"));
			q.append(isa.getSQLValue("ISA04"));
			q.append(isa.getSQLValue("ISA05"));
			q.append(isa.getSQLValue("ISA06"));
			q.append(isa.getSQLValue("ISA07"));
			q.append(isa.getSQLValue("ISA08"));
			q.append(getDateFromString(isa.getSQLValue("ISA09"))+", ");
			q.append(getTimeFromString(isa.getSQLValue("ISA10"))+", ");
			q.append(isa.getSQLValue("ISA11"));
			q.append(isa.getSQLValue("ISA12"));
			q.append(isa.getSQLValue("ISA13"));
			q.append(isa.getSQLValue("ISA14"));
			q.append(isa.getSQLValue("ISA15"));
			
			AmazonOrder.FunctionalGroupHeader gs = amazonOrder.functionalGroupHeader;
			q.append(gs.getSQLValue("GS01"));
			q.append(gs.getSQLValue("GS02"));
			q.append(gs.getSQLValue("GS03"));
			q.append(getDateFromString(gs.get("GS04"))+", ");
			q.append(getTimeFromString(gs.get("GS05"))+", ");
			q.append(getIntFromString(gs.get("GS06"))+", ");
			q.append(gs.getSQLValue("GS07"));
			q.append(gs.getSQLValue("GS08"));
			
			AmazonOrder.TransactionSetHeader st = amazonOrder.transactionSetHeader;
			q.append(st.getSQLValue("ST01"));
			q.append(st.getSQLValue("ST02"));
			
			AmazonOrder.BeginningSegmentForPurchaseOrder beg = amazonOrder.beginningSegmentForPurchaseOrder;
			q.append(beg.getSQLValue("BEG01"));
			q.append(beg.getSQLValue("BEG02"));
			q.append(beg.getSQLValue("BEG03"));
			q.append(getDateFromString(beg.get("BEG05"))+", ");
			
			AmazonOrder.Currency cur = amazonOrder.currency;
			q.append(cur.getSQLValue("CUR01"));
			q.append(cur.getSQLValue("CUR02"));

			AmazonOrder.ReferenceIdentification_CustomerOrderNumber ref_CON = amazonOrder.referenceIdentification_CustomerOrderNumber;
			q.append(ref_CON.getSQLValue("REF01"));
			q.append(ref_CON.getSQLValue("REF02"));
			
			if (amazonOrder.referenceIdentification_WebsiteCode != null) {
				AmazonOrder.ReferenceIdentification_WebsiteCode ref_WST = amazonOrder.referenceIdentification_WebsiteCode;
				q.append(ref_WST.getSQLValue("REF01"));
				q.append(ref_WST.getSQLValue("REF02"));
			}
			
			AmazonOrder.AmazonBillToLocation abtl = amazonOrder.amazonBillToLocations.get(0);
			q.append(abtl.getSQLValue("N101"));
			q.append(abtl.getSQLValue("N102"));
			q.append(abtl.getSQLValue("N103"));
			q.append(abtl.getSQLValue("N104"));
			
			AmazonOrder.ShipFrom sf = amazonOrder.vendorWarehouses.get(0);
			q.append(sf.getSQLValue("N101"));
			q.append(sf.getSQLValue("N102"));
			q.append(sf.getSQLValue("N103"));
			q.append(sf.getSQLValue("N104"));

			
			
			
			
			AmazonOrder.ShipToParty shipToParty = shipToPartyDetails.shipToParty;
			q.append(shipToParty.getSQLValue("N101"));
			q.append(shipToParty.getSQLValue("N102"));
			
			AmazonOrder.AdditionalNameInformation shipToAddNames = null;
			if (Utility.arrayIsNotEmpty(shipToPartyDetails.additionalNameInformation)) {
				shipToAddNames = shipToPartyDetails.additionalNameInformation.get(0);
				q.append(shipToAddNames.getSQLValue("N201"));
				q.append(shipToAddNames.getSQLValue("N202"));
			}
			
			AmazonOrder.AddressInformation shipToAddress = null;
			if (Utility.arrayIsNotEmpty(shipToPartyDetails.addressInformation)) {
				shipToAddress = shipToPartyDetails.addressInformation.get(0);
				q.append(shipToAddress.getSQLValue("N301"));
				q.append(shipToAddress.getSQLValue("N302"));
			}
			
			AmazonOrder.GeographicLocation geo = shipToPartyDetails.geographicLocation;
			q.append(geo.getSQLValue("N401"));
			q.append(geo.getSQLValue("N402"));
			q.append(geo.getSQLValue("N403"));
			q.append(geo.getSQLValue("N404"));
			q.append(geo.getSQLValue("N405"));
			q.append(geo.getSQLValue("N406"));
			
			AmazonOrder.CarrierDetails carrier = shipToPartyDetails.carrierDetails.get(0);
			q.append(carrier.getSQLValue("TD502"));
			q.append(carrier.getSQLValue("TD503"));
			q.append(carrier.getSQLValue("TD507"));
			q.append(carrier.getSQLValue("TD508"));
			

			AmazonOrder.CustomerBillToParty billToParty = customerBillToPartyDetails.customerBillToParty;
			q.append(billToParty.getSQLValue("N101"));
			q.append(billToParty.getSQLValue("N102"));
			
			if (Utility.arrayIsNotEmpty(customerBillToPartyDetails.additionalNameInformation)) {
				AmazonOrder.AdditionalNameInformation billToAddNames = customerBillToPartyDetails.additionalNameInformation.get(0);
				q.append(billToAddNames.getSQLValue("N201"));
				q.append(billToAddNames.getSQLValue("N202"));
			}
			
			if (Utility.arrayIsNotEmpty(customerBillToPartyDetails.addressInformation)) {
				AmazonOrder.AddressInformation billToAddress = customerBillToPartyDetails.addressInformation.get(0);
				q.append(billToAddress.getSQLValue("N301"));
				q.append(billToAddress.getSQLValue("N302"));
			}
			
			AmazonOrder.GeographicLocation billGeo = customerBillToPartyDetails.geographicLocation;
			q.append(billGeo.getSQLValue("N401"));
			q.append(billGeo.getSQLValue("N402"));
			q.append(billGeo.getSQLValue("N403"));
			q.append(billGeo.getSQLValue("N404"));
			
			if (customerBillToPartyDetails.administrativeCommunicationsContact_BillToCustomerInformation != null) {
				AmazonOrder.AdministrativeCommunicationsContact_BillToCustomerInformation adm = customerBillToPartyDetails.administrativeCommunicationsContact_BillToCustomerInformation;
				q.append(adm.getSQLValue("PER01"));
				q.append(adm.getSQLValue("PER02"));
				q.append(adm.getSQLValue("PER03"));
				q.append(adm.getSQLValue("PER04"));
				q.append(adm.getSQLValue("PER05"));
				q.append(adm.getSQLValue("PER06"));
			}

			
			AmazonOrder.TransactionTotals totals = amazonOrder.transactionTotalsDetails.get(0);
			q.append(getIntFromString(totals.get("CTT01"))+", ");
			q.append(getIntFromString(totals.get("CTT02"))+", ");
			
			AmazonOrder.TransactionSetTrailer tst = amazonOrder.transactionSetTrailer;
			q.append(getIntFromString(tst.get("SE01"))+", ");
			q.append(tst.getSQLValue("SE02"));
			
			AmazonOrder.FunctionalGroupTrailer fgt = amazonOrder.functionalGroupTrailer;
			q.append(getIntFromString(fgt.get("GE01"))+", ");
			q.append(getIntFromString(fgt.get("GE02"))+", ");
			
			AmazonOrder.InterchangeControlTrailer ict = amazonOrder.interchangeControlTrailer;
			q.append(getIntFromString(ict.get("IEA01"))+", ");
			q.append(getIntFromString(ict.get("IEA02")));
		
		q.append(")");
		
		String finishedSQL = q.toString();
		
		return finishedSQL;
	}
	
	
	
	
	
	public static String getDateFromString (String input) {
		String modifiedInput = cleanUp(input);
		String year = "";
		String month = "";
		String day = "";
		
		if (modifiedInput.length() == 6) {
			year = modifiedInput.substring(0, 2);
			month = modifiedInput.substring(2, 4);
			day = modifiedInput.substring(4, 6);
		}
		else if (modifiedInput.length() == 8) {
			year = modifiedInput.substring(0, 4);
			month = modifiedInput.substring(4, 6);
			day = modifiedInput.substring(6, 8);
		}
		
		return "\'"+ year +"-"+ month +"-"+ day +"\'";
	}
	
	
	public static String getTimeFromString (String input) {
		String modifiedInput = cleanUp(input);
		String hour = "";
		String minute = "";
		String second = "";
		
		if (modifiedInput.length() == 4) {
			hour = modifiedInput.substring(0, 2);
			minute = modifiedInput.substring(2, 4);
			second = "00";
		}
		else if (modifiedInput.length() == 6) {
			hour = modifiedInput.substring(0, 2);
			minute = modifiedInput.substring(2, 4);
			second = modifiedInput.substring(4, 6);
		}
		
		return "\'"+ hour +":"+ minute +":"+ second +"\'";
	}
	
	
	
	public static int getIntFromString (String input) {
		String modifiedInput = cleanUp(input);
		return Integer.parseInt(modifiedInput);
	}
	
	public static float getDecimalFromString (String input) {
		String modifiedInput = cleanUp(input);
		return Float.parseFloat(modifiedInput);
	}

	public static String removeEscapedQuotes (String input) {
		String modifiedInput = input;
		modifiedInput = modifiedInput.replace("\'", "");
		return modifiedInput;
	}
	
	public static String removeCommas (String input) {
		String modifiedInput = input;
		modifiedInput = modifiedInput.replace(",", "");
		return modifiedInput;
	}
	
	
	public static String cleanUp (String input) {
		String modifiedInput = removeEscapedQuotes(input);
		modifiedInput = removeCommas(modifiedInput).trim();
		modifiedInput = modifiedInput.trim();
		return modifiedInput;
	}
	
}
