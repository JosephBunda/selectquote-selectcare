package canpango.amazon;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import canpango.amazon.exceptions.AmazonDocumentParsingException;
import canpango.amazon.objects.AmazonOrder;
import canpango.amazon.objects.AmazonOrder.CustomerBillToPartyDetails;
import canpango.amazon.objects.AmazonOrder.ShipToPartyDetails;
import canpango.amazon.process.OrderParser;
import canpango.amazon.process.OrderToMySQL;
import canpango.connectors.MySQLConnector;
import canpango.connectors.SSHConnector;

import canpango.utiliy.Utility;

public class AmazonOrderParser {

	private static String configurationFilePath;
	
	private static boolean useSSH = true;
	private static boolean useMySQL = true;
	private static boolean verbose = true;
	
	private static SSHConnector 	ssh;
	private static MySQLConnector 	mySQL;
	
	private static String 	SSHHost;
	private static String 	SSHKeyFilePath;
	private static String 	SSHUsername;
	private static String 	SSHPassword;
	private static int		SSHLocalPort;
	private static int		SSHRemotePort;
	private static boolean	SSHStrictHostKeyCheck;
	
	private static String	MySQLHost;
	private static int		MySQLPort;
	private static String	MySQLSchema;
	private static String	MySQLUsername;
	private static String	MySQLPassword;
	private static boolean	MySQLEstablishNewConnection;
	
	private static String	documentToParse;
	
	private static boolean orderIsValid;
	
	
	public static void main (String[] args) {
		try {
			if (args != null && args.length > 0) {
				configurationFilePath = args[0];
				loadConfiguration(configurationFilePath);
			}
			else {
				throw new Exception("Error, no configuration file specified. Unable to execute.");
			}
		}
		catch (Exception ex) {
			Utility.printLine("Configuration file error: "+ ex.getMessage());
			return;
		}
		
		orderIsValid = false;
		
		if (useSSH) {
			try {
				ssh = new SSHConnector();
				ssh.verbose = verbose;
				ssh.openConnection(SSHHost, SSHKeyFilePath, SSHUsername, SSHPassword, SSHLocalPort, SSHRemotePort, SSHStrictHostKeyCheck);
			}
			catch (Exception exception ) {
				if (verbose) Utility.printLine("Couldn't start SSH session. Continuing on.");
			}
		}
		
		if (useMySQL) {
			try {
				mySQL = new MySQLConnector();
				mySQL.verbose = verbose;
				mySQL.openConnection(MySQLHost, MySQLPort, MySQLSchema, MySQLUsername, MySQLPassword, MySQLEstablishNewConnection);
			}
			catch (Exception exception) {
				if (verbose) Utility.printLine("Unable to establish connection to database. Will not be able to transmit contents of order. Continuing on.");
			}
		}
		
		
		AmazonOrder amazonOrder = null;
		
		try {
			String documentContent = loadDocumentContent(false);
			amazonOrder = executeParsing(documentContent, false);
		}
		catch (Exception exception) {
			if (verbose) Utility.printLine("Could not work with document. Please see the associated stack traces.");
		}
		
		if (verbose) Utility.printLine("");
		
		if (orderIsValid) {
			try {
				if (verbose) Utility.printLine("");
				if (verbose) Utility.printLine("Transmitting order to MySQL database...");
				
				OrderToMySQL.OrderInsertBatch orderInsertBatch = OrderToMySQL.createOrderInsertBatch(amazonOrder, verbose);
				
				int insertedHeaderRows = 0;
				
				try {
					insertedHeaderRows = mySQL.executeStatement(orderInsertBatch.insertOrderHeaderStatement);
				}
				catch (Exception cantInsertHeaderException) {
					if (verbose) Utility.printLine("Unable to insert header, skipping rest of order. Stack trace: ");
					if (verbose) Utility.printLine(Utility.getDetailedExceptionString(cantInsertHeaderException));
				}
				
				if (insertedHeaderRows == 1) {
					String[] batchedStatements = new String[2];
					batchedStatements[0] = orderInsertBatch.insertOrderMessagesStatement;
					batchedStatements[1] = orderInsertBatch.insertOrderLinesStatement;
					int affectedRows = insertedHeaderRows + mySQL.executeBatchStatements(batchedStatements);
					
					if (verbose) Utility.printLine("Success. Affected Rows: "+ affectedRows);
				}
				
				if (verbose) Utility.printLine("");
			} 
			catch (Exception e) {
				if (verbose) Utility.printLine("Error while transmitting order to MySQL database. Stack trace follows: ");
				if (verbose) Utility.printLine(Utility.getDetailedExceptionString(e));
			}
		}
		
		if (verbose) Utility.printLine("Execution complete, cleaning up...");
		
		if (useMySQL) {
			mySQL.closeConnection();
		}
		
		
		if (useSSH) {
			ssh.closeConnection();
		}
		
		if (verbose) Utility.printLine("Finished cleaning up. Goodbye!");
		
	}

	
	
	
	private static String loadDocumentContent (boolean verbose) throws Exception {
		String orderContent = "";
		String filePath = documentToParse;
		
		if (verbose) Utility.printLine("");
		if (verbose) Utility.printLine("Loading Amazon document at ["+ filePath +"]...");
		
		try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
		    StringBuilder sb = new StringBuilder();
		    String line = br.readLine();

		    while (line != null) {
		        sb.append(line);
		        sb.append(System.lineSeparator());
		        line = br.readLine();
		    }
		    
		    br.close();
		    orderContent = sb.toString();
		    
		    
		    if (verbose) Utility.printLine("Document read into memory.");
			if (verbose) Utility.printLine("");
		}
		catch (FileNotFoundException fileNotFoundException) {
			if (verbose) Utility.printLine("File not found.");
			throw new Exception(fileNotFoundException);
		} 
		catch (IOException ioException) {
			if (verbose) Utility.printLine("Unable to read file. Error and stack trace follows:");
			if (verbose) Utility.printLine(Utility.getDetailedExceptionString(ioException));
			throw new Exception(ioException);
		}
		
		
		
		return orderContent;
	}
	
	private static AmazonOrder executeParsing (String documentContent, boolean verbose) {
		AmazonOrder amazonOrder = null;
		
		if (Utility.isNotEmpty(documentContent)) {
			//Utility.printLine(orderContent);
			try {
				amazonOrder = OrderParser.parse("850", documentContent, verbose);
			} 
			catch (AmazonDocumentParsingException exception) {
				if (verbose) Utility.printLine("Error while parsing, stack trace follows: ");
				if (verbose) Utility.printLine(Utility.getDetailedExceptionString(exception));
			}
			
			
			if (amazonOrder != null) {
				if (orderIsValid = amazonOrder.verifyOrder()) {
					if (verbose) Utility.printLine("Order is valid.");
				}
				else {
					if (verbose) Utility.printLine("Order is invalid.");
				}
			}
		}
		else {
			if (verbose) Utility.printLine("No document content to parse.");
		}
	
		return amazonOrder;
	}
	
	
	private static void loadConfiguration (String configurationFilePath) throws Exception {
		try {
			if (verbose) Utility.printLine("");
			if (verbose) Utility.printLine("Loading configuration at ["+ configurationFilePath +"]...");
			
			try (BufferedReader br = new BufferedReader(new FileReader(configurationFilePath))) {
			    String line = br.readLine();

			    while (line != null) {
			    	int colonIndex = line.indexOf(":");
			    	if (colonIndex != -1) {
			    		String fieldName = line.substring(0, colonIndex);
			    		String fieldValue = line.substring(colonIndex+1, line.length());
			    		if (verbose) Utility.printLine("FIELD: "+fieldName+"     "+fieldValue);
			    		
			    		switch (fieldName) {
			    		case ("useSSH"):
			    			useSSH = Utility.parseBoolean(fieldValue);
			    			break;
			    		
			    		case ("useMySQL"):
			    			useMySQL = Utility.parseBoolean(fieldValue);
			    			break;
			    		
			    		case ("verbose"):
			    			verbose = Utility.parseBoolean(fieldValue);
			    			break;
			    		
			    		case ("SSHHost"):
			    			SSHHost = fieldValue;
			    			break;
			    		
			    		case ("SSHKeyFilePath"):
			    			SSHKeyFilePath = fieldValue;
			    			break;
			    		
			    		case ("SSHUsername"):
			    			SSHUsername = fieldValue;
			    			break;
			    		
			    		case ("SSHPassword"):
			    			SSHPassword = fieldValue;
			    			break;
			    			
			    		case ("SSHLocalPort"):
			    			SSHLocalPort = Utility.parseInt(fieldValue);
			    			break;

			    		case ("SSHRemotePort"):
			    			SSHRemotePort = Utility.parseInt(fieldValue);
			    			break;

			    		case ("SSHStrictHostKeyCheck"):
			    			SSHStrictHostKeyCheck = Utility.parseBoolean(fieldValue);
			    			break;
			    		
			    		case ("MySQLHost"):
			    			MySQLHost = fieldValue;
			    			break;
				    		
			    		case ("MySQLPort"):
			    			MySQLPort = Utility.parseInt(fieldValue);
			    			break;
				    		
			    		case ("MySQLSchema"):
			    			MySQLSchema = fieldValue;
			    			break;
				    		
			    		case ("MySQLUsername"):
			    			MySQLUsername = fieldValue;
			    			break;
				    		
			    		case ("MySQLPassword"):
			    			MySQLPassword = fieldValue;
			    			break;
				    		
			    		case ("MySQLEstablishNewConnection"):
			    			MySQLEstablishNewConnection = Utility.parseBoolean(fieldValue);
			    			break;
				    		
			    		case ("documentToParse"):
			    			documentToParse = fieldValue;
			    			break;
			    		
			    		}
			    	}
			        line = br.readLine();
			    }
			    
			    br.close();
			    
			    
			    if (verbose) Utility.printLine("Configuration read into memory.");
				if (verbose) Utility.printLine("");
			}
			catch (FileNotFoundException fileNotFoundException) {
				if (verbose) Utility.printLine("File not found.");
				throw new Exception(fileNotFoundException);
			} 
			catch (IOException ioException) {
				if (verbose) Utility.printLine("Unable to read file. Error and stack trace follows:");
				if (verbose) Utility.printLine(Utility.getDetailedExceptionString(ioException));
				throw new Exception(ioException);
			}
			
			/*
			useSSH 							= true;
			useMySQL 						= true;
			verbose 						= true;
			
			SSHHost							= "54.174.76.140";
			SSHKeyFilePath					= "/Users/josephbunda/.ssh/FourStar-Ubuntu.ppk";
			SSHUsername						= "ubuntu";
			SSHPassword						= "";
			SSHLocalPort					= 4321;
			SSHRemotePort					= 3306;
			SSHStrictHostKeyCheck			= false;
			
			MySQLHost						= "localhost";
			MySQLPort						= 4321;
			MySQLSchema						= "amazon_salesforce_queue";
			MySQLUsername					= "root";
			MySQLPassword					= "6420Aws";
			MySQLEstablishNewConnection		= false;
			
			documentToParse					= "/Users/josephbunda/Desktop/Example Fill Or Kill";
			*/
		}
		catch (Exception ex) {
			throw new Exception("There was a problem parsing the configuration document.");
		}
	}
	
	// Test Comment 2
}
