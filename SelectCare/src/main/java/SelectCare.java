import com.canpango.services.manywho.selectquote.selectcare.services.DataService;
import com.canpango.services.manywho.selectquote.selectcare.types.CallerInfo;
import com.canpango.services.manywho.selectquote.selectcare.types.Onboarder;
import com.canpango.services.manywho.selectquote.selectcare.types.MW_Onboarder;

import com.manywho.sdk.enums.ContentType;

public class SelectCare {

	public static void main(String[] args) {
		
		
		Onboarder onboarder = DataService.loadOnboarder("479a79f9-cadd-4a25-bf32-0125104434dc");
		System.out.println("Onboarder: "+ onboarder.Name);
		
		CallerInfo caller = DataService.loadCallerInfo("9999999999");
		System.out.println("Caller: "+ caller.Email);
		
	}

}
