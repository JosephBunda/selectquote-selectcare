package com.canpango.utility;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URLEncoder;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.HttpClients;

public class Utility {

	public static String getStringBodyFromResponse (HttpResponse response) throws IOException {
		String responseBody = "";
		
        HttpEntity entity = response.getEntity();

        if (entity != null) {
            InputStream instream = entity.getContent();
            try {
                StringWriter writer = new StringWriter();
                IOUtils.copy(instream, writer, "UTF-8");
                responseBody = writer.toString();
            } finally {
                instream.close();
            }
        }
        
        return responseBody;
	}
	
	
	
	
	
	public static HttpGet createHTTPGetRequest (String endpoint, String accessKey) {
		
		String encodedAccessKey = new String(Base64.encodeBase64(accessKey.getBytes()));
		
		HttpGet request = new HttpGet(endpoint);
        request.addHeader("Authorization", "Token "+encodedAccessKey);
        request.addHeader("Accept", "application/json");
        request.addHeader("Content-Type", "application/json");
        
        return request;
	}
	
	
	public static HttpPost createHTTPPostRequest (String endpoint, String accessKey) {
        return createHTTPPostRequest(endpoint, accessKey, null);
	}
	
	
	
	public static HttpPost createHTTPPostRequest (String endpoint, String accessKey, String requestBody) {
		
		String encodedAccessKey = new String(Base64.encodeBase64(accessKey.getBytes()));
		
        HttpPost request = new HttpPost(endpoint);
        request.addHeader("Authorization", "Token "+encodedAccessKey);
        request.addHeader("Accept", "application/json");
        request.addHeader("Content-Type", "application/json");
        
        if (accessKey != null) {
	        HttpEntity he = new ByteArrayEntity(requestBody.getBytes());
	        request.setEntity(he);
        }
        
        return request;
	}
	
	
	public static String getResponseBody (String method, String endpoint, String accessKey) throws Exception {
		return getResponseBody(method, endpoint, accessKey, null);
	}
	
	public static String getResponseBody (String method, String endpoint, String accessKey, String requestBody) throws Exception {
		try {			
			String modifiedEndpoint = encodeURL(endpoint);
			
			if (method.equals("GET")) {
				HttpGet request = createHTTPGetRequest(modifiedEndpoint, accessKey);
				HttpResponse response = HttpClients.createDefault().execute(request);
				
				switch (response.getStatusLine().getStatusCode()) {
				case (200):
					return Utility.getStringBodyFromResponse(response);
				case (403):
					throw new Exception("Credentials failed when authenticating with ["+ modifiedEndpoint +"], unable to retrieve data.");
				case (404):
					throw new Exception("Could not reach endpoint ["+ modifiedEndpoint +"], unable to retrieve data.");
				default:
					throw new Exception("An error occured. Response code: "+ response.getStatusLine().getStatusCode() +" Reason: "+ response.getStatusLine().getReasonPhrase());
				}
			}
			else if (method.equals("POST")) {
				HttpPost request = createHTTPPostRequest(modifiedEndpoint, accessKey, requestBody);
				HttpResponse response = HttpClients.createDefault().execute(request);
				
				switch (response.getStatusLine().getStatusCode()) {
				case (200):
					return Utility.getStringBodyFromResponse(response);
				case (403):
					throw new Exception("Credentials failed when authenticating with ["+ modifiedEndpoint +"], unable to retrieve data.");
				case (404):
					throw new Exception("Could not reach endpoint ["+ modifiedEndpoint +"], unable to retrieve data.");
				default:
					throw new Exception("An error occured. Response code: "+ response.getStatusLine().getStatusCode() +" Reason: "+ response.getStatusLine().getReasonPhrase());
				}
			}
			else {
				return null;
			}
		}
		catch (ClientProtocolException cpEx) {
			cpEx.printStackTrace();
			throw new Exception(cpEx.getMessage());
		}
		catch (IOException ioEx) {
			ioEx.printStackTrace();
			throw new Exception(ioEx.getMessage());
		}
		catch (Exception ex) {
			ex.printStackTrace();
			throw new Exception(ex.getMessage());
		}
	}
	
	
	
	public static String encodeURL (String url) throws Exception {
		String modifiedURL = "";
		try {
			int parameterIndex = url.indexOf("?");
			if (parameterIndex != -1) {
				parameterIndex++;
				String endpointBase = url.substring(0, parameterIndex);
				//System.out.println("Endpoint Base: "+ endpointBase);
				String parameters = url.substring(parameterIndex, url.length());
				//System.out.println("Parameters: "+ parameters);
				String[] parameterTokens = parameters.split("&");
				StringBuffer modifiedEndpointBuffer = new StringBuffer();
				modifiedEndpointBuffer.append(endpointBase);
				for (int i = 0; i < parameterTokens.length; i++) {
					String[] parts = parameterTokens[i].split("=");
					String name = parts[0];
					String value = parts[1];
					//System.out.println("P Name:"+ name);
					//System.out.println("P Value: "+ value);
					modifiedEndpointBuffer.append(name);
					modifiedEndpointBuffer.append("=");
					modifiedEndpointBuffer.append(URLEncoder.encode(value, "UTF-8"));
					if (i < parameterTokens.length) modifiedEndpointBuffer.append("&");
				}
				modifiedURL = modifiedEndpointBuffer.toString();
				//System.out.println("Encoded Endpoint: "+ modifiedURL);
			}
			else {
				modifiedURL = url;
			}
			
			return modifiedURL;
		}
		catch (Exception ex) {
			throw new Exception("Unable to properly encode URL: "+ url);
		}
	}
}
