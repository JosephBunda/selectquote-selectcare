package com.canpango.services.manywho.selectquote.selectcare.controllers;

import com.manywho.sdk.entities.describe.DescribeServiceResponse;
import com.manywho.sdk.entities.run.elements.config.ServiceRequest;
import com.manywho.sdk.entities.translate.Culture;
import com.manywho.sdk.services.describe.DescribeServiceBuilder;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.apache.log4j.Logger;

@Path("/")
@Consumes("application/json")
@Produces("application/json")
public class DescribeController {
	
	final static Logger logger = Logger.getLogger(DescribeController.class);

	
    @Path("/metadata")
    @POST
    public DescribeServiceResponse describe(ServiceRequest serviceRequest) throws Exception {
    	
    	DescribeServiceResponse response = null;
        try {
            response = new DescribeServiceBuilder()
                    .setProvidesDatabase(true)
                    .setProvidesLogic(true)
                    .setCulture(new Culture("EN", "US"))
                    .createDescribeService()
                    .createResponse();
        }
        catch (Exception ex) {
        	logger.info("ERROR: "+ex.getMessage());
        }

        return response;
    }
}
