package com.canpango.services.manywho.selectquote.selectcare.types;

import com.manywho.sdk.entities.draw.elements.type.TypeElementBinding;
import com.manywho.sdk.entities.draw.elements.type.TypeElementBindingCollection;
import com.manywho.sdk.entities.draw.elements.type.TypeElementProperty;
import com.manywho.sdk.entities.draw.elements.type.TypeElementPropertyBinding;
import com.manywho.sdk.entities.draw.elements.type.TypeElementPropertyBindingCollection;
import com.manywho.sdk.entities.draw.elements.type.TypeElementPropertyCollection;
import com.manywho.sdk.enums.ContentType;
import com.manywho.sdk.services.describe.types.AbstractType;

public class FlexibleDates /*extends AbstractType*/ {
	public static final String NAME = "FlexibleDates";

	
	public String	StartDateTime;
	public String	EndDateTime;
	
	/*
    @Override
    public String getDeveloperName() {
        return NAME;
    }

    @Override
    public TypeElementBindingCollection getBindings() {
        return new TypeElementBindingCollection() {{
            add(new TypeElementBinding("FlexibleDates", "Flexible dates that represent the enrollment window for a medical supplement plan, which change yearly.", getDeveloperName(), new TypeElementPropertyBindingCollection() {{
                add(new TypeElementPropertyBinding("StartDateTime", "StartDateTime"));
                add(new TypeElementPropertyBinding("EndDateTime", 	"EndDateTime"));
            }}));
        }};
    }

    @Override
    public TypeElementPropertyCollection getProperties() {
        return new TypeElementPropertyCollection() {{
            add(new TypeElementProperty("StartDateTime", 	ContentType.DateTime));
            add(new TypeElementProperty("EndDateTime", 		ContentType.DateTime));
        }};
    }
    */
}
