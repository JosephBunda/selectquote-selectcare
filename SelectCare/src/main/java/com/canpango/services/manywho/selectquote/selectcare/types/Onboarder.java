package com.canpango.services.manywho.selectquote.selectcare.types;

public class Onboarder {
	public String ID;
	public String Name;
	public String Phone;
	public String Email;
	public String AssignedUser;
	public String OnboardingPhoneNumber;
	public String CarrierUnderwritingPhone;
}
