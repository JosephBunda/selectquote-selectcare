package com.canpango.services.manywho.selectquote.selectcare.types;

import com.manywho.sdk.entities.draw.elements.type.TypeElementBinding;
import com.manywho.sdk.entities.draw.elements.type.TypeElementBindingCollection;
import com.manywho.sdk.entities.draw.elements.type.TypeElementProperty;
import com.manywho.sdk.entities.draw.elements.type.TypeElementPropertyBinding;
import com.manywho.sdk.entities.draw.elements.type.TypeElementPropertyBindingCollection;
import com.manywho.sdk.entities.draw.elements.type.TypeElementPropertyCollection;
import com.manywho.sdk.enums.ContentType;
import com.manywho.sdk.services.describe.types.AbstractType;

public class CallInfo /*extends AbstractType*/ {
	public static final String NAME = "CallInfo";
	
	
	public String 	DateAndTime;
	public String	CallerID;
	public String 	CallerName;
	public String	MSPQuestions;
	public String	CarrierPhoneInterviewCompleted;
	public String	HadTimeToCompleteInterview;
	public String	CallAction;
	public String 	Status;
	public String	PDPEnrolled;
	public String	InterestedInProductSpecialistTalk;
	public String	CallbackDateTime;
	public String	SelectedProductSpecialist;
	
	
/*
    @Override
    public String getDeveloperName() {
        return NAME;
    }

    @Override
    public TypeElementBindingCollection getBindings() {
        return new TypeElementBindingCollection() {{
            add(new TypeElementBinding("CallInfo", "Holds information concerning a call.", getDeveloperName(), new TypeElementPropertyBindingCollection() {{
                add(new TypeElementPropertyBinding("DateAndTime", 						"DateAndTime"));
                add(new TypeElementPropertyBinding("CallerID", 							"CallerID"));
                add(new TypeElementPropertyBinding("CallerName", 						"CallerName"));
                add(new TypeElementPropertyBinding("MSPQuestions", 						"MSPQuestions"));
                add(new TypeElementPropertyBinding("CarrierPhoneInterviewCompleted", 	"CarrierPhoneInterviewCompleted"));
                add(new TypeElementPropertyBinding("HadTimeToCompleteInterview", 		"HadTimeToCompleteInterview"));
                add(new TypeElementPropertyBinding("CallAction", 						"CallAction"));
                add(new TypeElementPropertyBinding("Status", 							"Status"));
                add(new TypeElementPropertyBinding("PDPEnrolled", 						"PDPEnrolled"));
                add(new TypeElementPropertyBinding("InterestedInProductSpecialistTalk", "InterestedInProductSpecialistTalk"));
                add(new TypeElementPropertyBinding("CallbackDateTime", 					"CallbackDateTime"));
                add(new TypeElementPropertyBinding("SelectedProductSpecialist", 		"SelectedProductSpecialist"));
            }}));
        }};
    }

    @Override
    public TypeElementPropertyCollection getProperties() {
        return new TypeElementPropertyCollection() {{
            add(new TypeElementProperty("DateAndTime", 							ContentType.DateTime));
            add(new TypeElementProperty("CallerID", 							ContentType.String));
            add(new TypeElementProperty("CallerName", 							ContentType.String));
            add(new TypeElementProperty("MSPQuestions", 						ContentType.Boolean));
            add(new TypeElementProperty("CarrierPhoneInterviewCompleted", 		ContentType.Boolean));
            add(new TypeElementProperty("HadTimeToCompleteInterview", 			ContentType.Boolean));
            add(new TypeElementProperty("CallAction", 							ContentType.String));
            add(new TypeElementProperty("Status", 								ContentType.String));
            add(new TypeElementProperty("PDPEnrolled",							ContentType.Boolean));
            add(new TypeElementProperty("InterestedInProductSpecialistTalk",	ContentType.Boolean));
            add(new TypeElementProperty("CallbackDateTime", 					ContentType.DateTime));
            add(new TypeElementProperty("SelectedProductSpecialist", 			ContentType.Boolean));
        }};
    }
    */
}
