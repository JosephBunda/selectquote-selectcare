package com.canpango.services.manywho.selectquote.selectcare.types;

import com.manywho.sdk.entities.draw.elements.type.TypeElementBinding;
import com.manywho.sdk.entities.draw.elements.type.TypeElementBindingCollection;
import com.manywho.sdk.entities.draw.elements.type.TypeElementProperty;
import com.manywho.sdk.entities.draw.elements.type.TypeElementPropertyBinding;
import com.manywho.sdk.entities.draw.elements.type.TypeElementPropertyBindingCollection;
import com.manywho.sdk.entities.draw.elements.type.TypeElementPropertyCollection;
import com.manywho.sdk.enums.ContentType;
import com.manywho.sdk.services.describe.types.AbstractType;

public class CallerInfo /*extends AbstractType*/ {
	public static final String NAME = "CallerInfo";

	
	public String ID;
	public String Name;
	public String DayPhone;
	public String EveningPhone;
	public String CellPhone;
	public String Email;
	public String GuaranteedIssuance;
	public String CarrierName;
	public String CarrierPlanName;
	public String EffectiveDateStart;
	public String EffectiveDateEnd;
	
	
	/*
    @Override
    public String getDeveloperName() {
        return NAME;
    }

    @Override
    public TypeElementBindingCollection getBindings() {
        return new TypeElementBindingCollection() {{
            add(new TypeElementBinding("CallerInfo", "Holds information concerning the active caller.", getDeveloperName(), new TypeElementPropertyBindingCollection() {{
                add(new TypeElementPropertyBinding("ID", 					"ID"));
                add(new TypeElementPropertyBinding("Name", 					"Name"));
                add(new TypeElementPropertyBinding("DayPhone", 				"DayPhone"));
                add(new TypeElementPropertyBinding("EveningPhone", 			"EveningPhone"));
                add(new TypeElementPropertyBinding("CellPhone", 			"CellPhone"));
                add(new TypeElementPropertyBinding("Email", 				"Email"));
                add(new TypeElementPropertyBinding("GuaranteedIssuance", 	"GuaranteedIssuance"));
                add(new TypeElementPropertyBinding("CarrierName", 			"CarrierName"));
                add(new TypeElementPropertyBinding("CarrierPlanName", 		"CarrierPlanName"));
                add(new TypeElementPropertyBinding("EffectiveDateStart", 	"EffectiveDateStart"));
                add(new TypeElementPropertyBinding("EffectiveDateEnd", 		"EffectiveDateEnd"));
            }}));
        }};
    }

    @Override
    public TypeElementPropertyCollection getProperties() {
        return new TypeElementPropertyCollection() {{
            add(new TypeElementProperty("ID", 						ContentType.String));
            add(new TypeElementProperty("Name", 					ContentType.String));
            add(new TypeElementProperty("DayPhone", 				ContentType.String));
            add(new TypeElementProperty("EveningPhone", 			ContentType.String));
            add(new TypeElementProperty("CellPhone", 				ContentType.String));
            add(new TypeElementProperty("Email", 					ContentType.String));
            add(new TypeElementProperty("GuaranteedIssuance", 		ContentType.Boolean));
            add(new TypeElementProperty("CarrierName", 				ContentType.String));
            add(new TypeElementProperty("CarrierUnderwritingPhone", ContentType.String));
            add(new TypeElementProperty("CarrierPlanName", 			ContentType.String));
            add(new TypeElementProperty("EffectiveDateStart", 		ContentType.DateTime));
            add(new TypeElementProperty("EffectiveDateEnd", 		ContentType.DateTime));
        }};
    }
    */
}
