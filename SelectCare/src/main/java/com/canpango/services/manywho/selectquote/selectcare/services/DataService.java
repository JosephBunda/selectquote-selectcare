package com.canpango.services.manywho.selectquote.selectcare.services;

import com.manywho.sdk.entities.run.elements.type.ObjectCollection;
import com.manywho.sdk.entities.run.elements.type.ObjectDataResponse;
import com.manywho.sdk.entities.run.elements.type.Property;
import com.manywho.sdk.entities.run.elements.type.PropertyCollection;
import com.canpango.services.manywho.selectquote.selectcare.configuration.Configuration;
import com.canpango.services.manywho.selectquote.selectcare.types.CallerInfo;
import com.canpango.services.manywho.selectquote.selectcare.types.Onboarder;
import com.canpango.services.manywho.selectquote.selectcare.types.MW_Onboarder;
import com.canpango.utility.Utility;







import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class DataService {
	
	private static final String accessKey 		= "BA9BBFE8-F749-4E7E-B635-1462F99E31C7";
	private static final String endpointBase 	= "http://odata-dev.selectquotesenior.com/SelectCare.svc/";
	
    public ObjectDataResponse loadExamples(Configuration configurationValues) {
        // Load some data from a database or API, then convert back into ManyWho Objects (ObjectCollection)

        return new ObjectDataResponse();
    }

    public ObjectDataResponse saveExample(Configuration configurationValues, ObjectCollection objectData) {
        // Save some data to a database or API

        return new ObjectDataResponse();
    }
    
    
    public ObjectDataResponse getOnboarder (String onboardingUserID) {
    	Onboarder onboarder = loadOnboarder(onboardingUserID);
    	
    	com.manywho.sdk.entities.run.elements.type.Object manyWhoOnboarderObject = new com.manywho.sdk.entities.run.elements.type.Object();
    	manyWhoOnboarderObject.setDeveloperName(MW_Onboarder.DEVELOPER_NAME);
    	
    	manyWhoOnboarderObject.setExternalId(onboarder.ID);
    	
    	PropertyCollection properties = new PropertyCollection();
    	properties.add(new Property("ID", 						onboarder.ID));
    	properties.add(new Property("Name", 					onboarder.Name));
    	properties.add(new Property("Phone", 					onboarder.Phone));
    	properties.add(new Property("Email", 					onboarder.Email));
    	properties.add(new Property("AssignedUser", 			onboarder.AssignedUser));
    	properties.add(new Property("OnboardingPhoneNumber", 	onboarder.OnboardingPhoneNumber));
    	properties.add(new Property("CarrierUnderwritingPhone",	onboarder.CarrierUnderwritingPhone));
    	
    	manyWhoOnboarderObject.setProperties(properties);
    	ObjectDataResponse objectDataResponse = new ObjectDataResponse();
    	ObjectCollection objectCollection = new ObjectCollection();
    	objectCollection.add(manyWhoOnboarderObject);
    	objectDataResponse.setObjectData(objectCollection);
    	
    	return objectDataResponse;
    }
    
    public static Onboarder loadOnboarder (String onboardingUserID) {
    	// Todo: Relocate these properties to a resource file or other external storage to avoid hard-coding once development is completed.
        String endpoint = endpointBase+"users?$filter=usr_onboard_flag eq true and usr_key eq guid'"+ onboardingUserID +"'";
        

        try {
        	String responseBody = Utility.getResponseBody("GET", endpoint, accessKey);
        	System.out.println("RESPONSE: "+responseBody);
        	// Parse the response body for information.
	        JSONParser 	jsonParser 		= new JSONParser();
	        JSONObject 	jsonRoot 		= (JSONObject) jsonParser.parse(responseBody);
	        JSONArray	jsonArray		= (JSONArray) jsonRoot.get("value");
	        JSONObject 	jsonObject 		= (JSONObject) jsonArray.get(0);
	        
	        
	        Onboarder onboarder = new Onboarder();
	        
	        onboarder.ID	 				= (String) jsonObject.get("usr_key");
	        onboarder.Name					= (String) jsonObject.get("usr_first_name") +" "+ (String) jsonObject.get("usr_last_name");
	        onboarder.OnboardingPhoneNumber	= (String) jsonObject.get("usr_softphone_sq_general");
	        onboarder.Phone	 				= (String) jsonObject.get("usr_softphone_sq_personal");
	        
	        /*
	        JSONArray specialistAgents = (JSONArray) jsonObject.get("SpecialistAgents");
	        Iterator itr = specialistAgents.iterator();
	        while (itr.hasNext()) {
	        	JSONObject child = (JSONObject) itr.next();
	        	String specialistName 	= (String) child.get("Name");
	        	String specialistPhone 	= (String) child.get("Phone");
	        }
	        */
	        
	        return onboarder;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    
    public static CallerInfo loadCallerInfo (String callerNumber) {
    	// Todo: Relocate these properties to a resource file or other external storage to avoid hard-coding once development is completed.
        String endpoint = endpointBase+"Individuals?$top=1&filter=indv_day_phone eq "+ callerNumber +" or indv_evening_phone eq "+ callerNumber +" or indv_cell_phone eq "+ callerNumber +" or indv_inbound_phone eq "+ callerNumber;
        

        try {
        	String responseBody = Utility.getResponseBody("GET", endpoint, accessKey);
        	System.out.println("RESPONSE: "+responseBody);
        	// Parse the response body for information.
        	JSONParser 	jsonParser 		= new JSONParser();
	        JSONObject 	jsonRoot 		= (JSONObject) jsonParser.parse(responseBody);
	        JSONArray	jsonArray		= (JSONArray) jsonRoot.get("value");
	        JSONObject 	jsonObject 		= (JSONObject) jsonArray.get(0);
	        
	        
	        CallerInfo caller = new CallerInfo();
	        
	        caller.ID	 				= (String) jsonObject.get("indv_key");
	        caller.Name					= (String) jsonObject.get("indv_first_name") +" "+ (String) jsonObject.get("indv_last_name");
	        caller.DayPhone				= (String) jsonObject.get("indv_day_phone");
	        caller.EveningPhone			= (String) jsonObject.get("indv_evening_phone");
	        caller.CellPhone			= (String) jsonObject.get("indv_cell_phone");
	        caller.Email				= (String) jsonObject.get("indv_email");
	        caller.GuaranteedIssuance	= "";
	        caller.CarrierName			= "";
	        caller.CarrierPlanName		= "";
	        caller.EffectiveDateStart   = "";
	        caller.EffectiveDateEnd		= "";
	        
	        /*
	        JSONArray specialistAgents = (JSONArray) jsonObject.get("SpecialistAgents");
	        Iterator itr = specialistAgents.iterator();
	        while (itr.hasNext()) {
	        	JSONObject child = (JSONObject) itr.next();
	        	String specialistName 	= (String) child.get("Name");
	        	String specialistPhone 	= (String) child.get("Phone");
	        }
	        */
	        
	        return caller;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    /*
    private void loadCallerInfo (int callerID) {
    	// Todo: Relocate these properties to a resource file or other external storage to avoid hard-coding once development is completed.
        String endpoint         = endpointBase+"WorkflowBatch.svc/SaveBatch";
        
        // Breaking standard formatting to show the "hierarchy" of this json body better.
        StringBuffer j = new StringBuffer();
        j.append("{");
            j.append("\"Caller\": {");
                    j.append("\"Phone\": \""+ callerID +"\"");
            j.append("}");
        j.append("}");
        String requestBody = j.toString();
        
        
        String responseBody = "";

        try {
        	responseBody = Utility.getResponseBody(endpoint, accessKey, requestBody);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        
        
        try {
	        // Parse the response body for information.
	        JSONParser jsonParser = new JSONParser();
	        JSONObject jsonObject = (JSONObject) jsonParser.parse(responseBody);
	        
	        String ID	 				= (String) jsonObject.get("ID");
	        String name 				= (String) jsonObject.get("Name");
	        String phone		 		= (String) jsonObject.get("Phone");
	        String directPhone			= (String) jsonObject.get("DirectPhone");
	        String guaranteedIssuance	= (String) jsonObject.get("GuaranteedIssuance");
	        
	        JSONObject carrierPlan = (JSONObject) jsonObject.get("CarrierPlan");
	        String planID 				= (String) jsonObject.get("ID");
	        String planName 			= (String) jsonObject.get("Name");
	        String carrierName	 		= (String) jsonObject.get("CarrierName");
	        String effectiveDateStart	= (String) jsonObject.get("EffectiveDateStart");
	        String effectiveDateEnd		= (String) jsonObject.get("EffectiveDateEnd");
	        
        }
        catch (Exception ex) {
        	
        }
        
    }
    
    
    
    
    
    private void loadCarrierPlan (int carrierPlanID) {
    	// Todo: Relocate these properties to a resource file or other external storage to avoid hard-coding once development is completed.
        String endpoint         = endpointBase+"WorkflowBatch.svc/SaveBatch";
        
        // Breaking standard formatting to show the "hierarchy" of this json body better.
        StringBuffer j = new StringBuffer();
        j.append("{");
            j.append("\"CarrierPlan\": {");
                    j.append("\"ID\": \""+ carrierPlanID +"\"");
            j.append("}");
        j.append("}");
        String requestBody = j.toString();
        
        
        String responseBody = "";

        try {
        	responseBody = Utility.getResponseBody(endpoint, accessKey, requestBody);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        
        
        try {
	        // Parse the response body for information.
	        JSONParser jsonParser = new JSONParser();
	        JSONObject jsonObject = (JSONObject) jsonParser.parse(responseBody);
	        
	        String ID	 				= (String) jsonObject.get("ID");
	        String name 				= (String) jsonObject.get("Name");
	        String carrierName	 		= (String) jsonObject.get("CarrierName");
	        String effectiveDateStart	= (String) jsonObject.get("EffectiveDateStart");
	        String effectiveDateEnd		= (String) jsonObject.get("EffectiveDateEnd");
        }
        catch (Exception ex) {
        	
        }
        
    }
    
    
    
    private void loadCarrier (int carrierID) {
    	// Todo: Relocate these properties to a resource file or other external storage to avoid hard-coding once development is completed.
        String endpoint         = endpointBase+"WorkflowBatch.svc/SaveBatch";
        
        // Breaking standard formatting to show the "hierarchy" of this json body better.
        StringBuffer j = new StringBuffer();
        j.append("{");
            j.append("\"Carrier\": {");
                    j.append("\"ID\": \""+ carrierID +"\"");
            j.append("}");
        j.append("}");
        String requestBody = j.toString();
        
        
        String responseBody = "";

        try {
        	responseBody = Utility.getResponseBody(endpoint, accessKey, requestBody);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        
        
        try {
	        // Parse the response body for information.
	        JSONParser jsonParser = new JSONParser();
	        JSONObject jsonObject = (JSONObject) jsonParser.parse(responseBody);
	        
	        String ID	 				= (String) jsonObject.get("ID");
	        String name 				= (String) jsonObject.get("Name");
	        String underwritingPhone	= (String) jsonObject.get("UnderwritingPhone");
        }
        catch (Exception ex) {
        	
        }
        
    }
    
    
    
    
    private void loadFlexiblePDPEnrollmentDates (int inputYear) {
    	// Todo: Relocate these properties to a resource file or other external storage to avoid hard-coding once development is completed.
        String endpoint         = endpointBase+"WorkflowBatch.svc/SaveBatch";
        
        // Breaking standard formatting to show the "hierarchy" of this json body better.
        StringBuffer j = new StringBuffer();
        j.append("{");
            j.append("\"PDPEnrollDates\": {");
                    j.append("\"Year\": \""+ inputYear +"\"");
            j.append("}");
        j.append("}");
        String requestBody = j.toString();
        
        
        String responseBody = "";

        try {
        	responseBody = Utility.getResponseBody(endpoint, accessKey, requestBody);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        
        
        try {
	        // Parse the response body for information.
	        JSONParser jsonParser = new JSONParser();
	        JSONObject jsonObject = (JSONObject) jsonParser.parse(responseBody);
	        
	        String year	 				= (String) jsonObject.get("Year");
	        String start 				= (String) jsonObject.get("Start");
	        String end					= (String) jsonObject.get("End");
        }
        catch (Exception ex) {
        	
        }
        
    }
    
    
    
    
    
    
    
    private void saveCallData (String callDataObject) {
    	// Todo: Relocate these properties to a resource file or other external storage to avoid hard-coding once development is completed.
        String endpoint         = endpointBase+"WorkflowBatch.svc/SaveBatch";
        
        // Breaking standard formatting to show the "hierarchy" of this json body better.
        StringBuffer j = new StringBuffer();
        j.append("{");
            j.append("\"CallInformation\": {");
                    j.append("\"DateTime\": \""+ "" +"\"");
                    j.append("\"CallerID\": \""+ "" +"\"");
                    j.append("\"CallerName\": \""+ "" +"\"");
                    j.append("\"MSPQuestions\": \""+ "" +"\"");
                    j.append("\"CarrierPhoneInterviewCompleted\": \""+ "" +"\"");
                    j.append("\"HadTimeToCompleteInterview\": \""+ "" +"\"");
                    j.append("\"CallAction\": \""+ "" +"\"");
                    j.append("\"Status\": \""+ "" +"\"");
                    j.append("\"PDPEnrolled\": \""+ "" +"\"");
                    j.append("\"InterestedInProductSpecialistTalk\": \""+ "" +"\"");
                    j.append("\"CallbackDateTime\": \""+ "" +"\"");
                    j.append("\"SelectedProductSpecialist\": \""+ "" +"\"");
            j.append("}");
        j.append("}");
        String requestBody = j.toString();
        
        
        String responseBody = "";

        try {
        	responseBody = Utility.getResponseBody(endpoint, accessKey, requestBody);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        
        
        
        
    }
    */
    
    /*
    private void sendQuestionAndAnswerToSelectQuote () {

        // Todo: Relocate these properties to a resource file or other external storage to avoid hard-coding once development is completed.
        String accessKey        = "BA9BBFE8-F749-4E7E-B635-1462F99E31C7";
        String encodedAccessKey = new String(Base64.encodeBase64(accessKey.getBytes()));
        String contentType      = "application/json";
        String endpoint         = "https://sqs-scqa.condadogroup.com/corvisa/WorkflowBatch.svc/SaveBatch";

        // Breaking standard formatting to show the "hierarchy" of this json body better.
        StringBuffer j = new StringBuffer();
        
        j.append("{");
            j.append("\"Workflows\": [");
                j.append("{");
                    j.append("\"Title\": \""+ questionAnswerCombos.get(0).flowTitle +"\",");
                    j.append("\"Values\": [");

                        for (int i = 0; i < questionAnswerCombos.size(); i++) {
                            QuestionAnswerComboType qac = questionAnswerCombos.get(i);
                            if (i > 0) j.append(",");
                            j.append("{\"Name\": \"" + qac.question + "\", \"Value\": \"" + qac.answer + "\", \"AccountID\": \"" + qac.userID + "\"}");
                        }

                    j.append("]");
                j.append("}");
            j.append("]");
        j.append("}");
		
        
        // Create and send a request to SelectQuote's service.
        HttpClient httpClient = HttpClients.createDefault();
        HttpPost request = new HttpPost(endpoint);
        request.addHeader("Authorization", "Token "+encodedAccessKey);
        request.addHeader("Accept", "application/json");
        request.addHeader("Content-Type", "application/json");
        HttpEntity he = new ByteArrayEntity(j.toString().getBytes());
        request.setEntity(he);

        String responseBody = "";

        try {
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();


            if (entity != null) {
                InputStream instream = entity.getContent();
                try {
                    StringWriter writer = new StringWriter();
                    IOUtils.copy(instream, writer, "UTF-8");
                    responseBody = writer.toString();
                } finally {
                    instream.close();
                }
            }
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }

        // Todo: process the response body for error handling and/or messages
    }
    */
    
    
}
