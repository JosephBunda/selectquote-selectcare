package com.canpango.services.manywho.selectquote.selectcare.actions;

import com.manywho.sdk.entities.describe.DescribeValue;
import com.manywho.sdk.entities.describe.DescribeValueCollection;
import com.manywho.sdk.enums.ContentType;
import com.manywho.sdk.services.describe.actions.AbstractAction;
import com.canpango.services.manywho.selectquote.selectcare.types.MW_Onboarder;

public class OnboarderAction extends AbstractAction {
    @Override
    public String getUriPart() {
        return "SelectCare/Onboarder";
    }

    @Override
    public String getDeveloperName() {
        return "Onboarder";
    }

    @Override
    public String getDeveloperSummary() {
        return "Retrieves an onboarder's information.";
    }

    @Override
    public DescribeValueCollection getServiceInputs() {
        return new DescribeValueCollection() {{
            add(new DescribeValue(MW_Onboarder.DEVELOPER_NAME, ContentType.String, true, null, MW_Onboarder.DEVELOPER_NAME));
        }};
    }

    @Override
    public DescribeValueCollection getServiceOutputs() {
        return new DescribeValueCollection() {{
            add(new DescribeValue(MW_Onboarder.DEVELOPER_NAME, ContentType.Object, true, null, MW_Onboarder.DEVELOPER_NAME));
        }};
    }
}
