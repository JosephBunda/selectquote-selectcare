package com.canpango.services.manywho.selectquote.selectcare.controllers;

import com.manywho.sdk.entities.run.elements.type.ObjectCollection;
import com.manywho.sdk.entities.run.elements.type.ObjectDataRequest;
import com.manywho.sdk.entities.run.elements.type.ObjectDataResponse;
import com.manywho.sdk.entities.run.elements.type.Property;
import com.manywho.sdk.entities.run.elements.type.PropertyCollection;
import com.manywho.sdk.enums.ContentType;
import com.manywho.sdk.services.controllers.AbstractDataController;
import com.canpango.services.manywho.selectquote.selectcare.configuration.Configuration;
import com.canpango.services.manywho.selectquote.selectcare.services.DataService;
import com.canpango.services.manywho.selectquote.selectcare.types.MW_Onboarder;
import com.canpango.services.manywho.selectquote.selectcare.types.Onboarder;

import javax.inject.Inject;
import javax.ws.rs.*;


@Path("/")
@Consumes("application/json")
@Produces("application/json")
public class DataController extends AbstractDataController {
    @Inject
    private DataService dataService;

    @Override
    public ObjectDataResponse delete(ObjectDataRequest objectDataRequest) throws Exception {
        return null;
    }

    @Override
    public ObjectDataResponse load(ObjectDataRequest objectDataRequest) throws Exception {
        //Configuration configurationValues = this.parseConfigurationValues(objectDataRequest, Configuration.class);
        ObjectCollection objectCollection = objectDataRequest.getObjectData();
        
        
        String objectDeveloperName = objectDataRequest.getObjectDataType().getDeveloperName();

        switch (objectDeveloperName) {
            case MW_Onboarder.DEVELOPER_NAME:
            	com.manywho.sdk.entities.run.elements.type.Object onboarderInfoObject = (com.manywho.sdk.entities.run.elements.type.Object) objectCollection.get(0);
            	String onboarderID = getStringValue(onboarderInfoObject.getProperties(), MW_Onboarder.ID_FIELD);
            	
                return this.dataService.getOnboarder(onboarderID);
                
        }

        return new ObjectDataResponse();
    }

    @Override
    public ObjectDataResponse save(ObjectDataRequest objectDataRequest) throws Exception {
        Configuration configurationValues = this.parseConfigurationValues(objectDataRequest, Configuration.class);

        String objectDeveloperName = objectDataRequest.getObjectDataType().getDeveloperName();

        switch (objectDeveloperName) {
            case MW_Onboarder.DEVELOPER_NAME:
                return this.dataService.saveExample(configurationValues, objectDataRequest.getObjectData());
        }

        return new ObjectDataResponse();
    }
    
    
    
    
    private String getStringValue (PropertyCollection propertyCollection, String propertyName) {
    	for (Property prop : propertyCollection) {
            String      name        = prop.getDeveloperName();
            ContentType contentType = prop.getContentType();
            String      value       = prop.getContentValue();

            if (contentType == ContentType.String && name.equals(propertyName))    {
            	return value;
            }
        }
    	
    	return null;
    }
}
