package com.canpango.services.manywho.selectquote.selectcare.types;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.manywho.sdk.entities.draw.elements.type.TypeElementBinding;
import com.manywho.sdk.entities.draw.elements.type.TypeElementBindingCollection;
import com.manywho.sdk.entities.draw.elements.type.TypeElementProperty;
import com.manywho.sdk.entities.draw.elements.type.TypeElementPropertyBinding;
import com.manywho.sdk.entities.draw.elements.type.TypeElementPropertyBindingCollection;
import com.manywho.sdk.entities.draw.elements.type.TypeElementPropertyCollection;
import com.manywho.sdk.enums.ContentType;
import com.manywho.sdk.services.describe.types.AbstractType;

public class MW_Onboarder extends AbstractType {
	public static final String DEVELOPER_NAME 					= "Onboarder";
	
	public static final String ID_FIELD 						= "ID";
	public static final String NAME_FIELD 						= "Name";
	public static final String PHONE_FIELD 						= "Phone";
	public static final String EMAIL_FIELD 						= "Email";
	public static final String ASSIGNED_USER_FIELD 				= "AssignedUser";
	public static final String ONBOARDING_PHONE_FIELD 			= "OnboardingPhoneNumber";
	public static final String CARRIER_UNDERWRITING_PHONE_FIELD = "CarrierUnderwritingPhone";
	
    @Override
    public String getDeveloperName() {
        return DEVELOPER_NAME;
    }
    
    @Override
    public String getServiceElementId () {
    	return DEVELOPER_NAME;
    }

    @Override
    public TypeElementBindingCollection getBindings() {
        return new TypeElementBindingCollection() {{
            add(new TypeElementBinding(DEVELOPER_NAME, "Holds information concerning the onboarding specialist.", getDeveloperName(), new TypeElementPropertyBindingCollection() {{
                add(new TypeElementPropertyBinding(ID_FIELD, 							ID_FIELD));
                add(new TypeElementPropertyBinding(NAME_FIELD, 							NAME_FIELD));
                add(new TypeElementPropertyBinding(PHONE_FIELD, 						PHONE_FIELD));
                add(new TypeElementPropertyBinding(EMAIL_FIELD, 						EMAIL_FIELD));
                add(new TypeElementPropertyBinding(ASSIGNED_USER_FIELD, 				ASSIGNED_USER_FIELD));
                add(new TypeElementPropertyBinding(ONBOARDING_PHONE_FIELD, 				ONBOARDING_PHONE_FIELD));
                add(new TypeElementPropertyBinding(CARRIER_UNDERWRITING_PHONE_FIELD, 	CARRIER_UNDERWRITING_PHONE_FIELD));
            }}));
        }};
    }

    @Override
    public TypeElementPropertyCollection getProperties() {
    	ObjectMapper objectMapper = new ObjectMapper();
    	objectMapper.configure(SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
        return new TypeElementPropertyCollection() {{
            add(new TypeElementProperty(ID_FIELD, 							ContentType.String,		ID_FIELD));
            add(new TypeElementProperty(NAME_FIELD, 						ContentType.String,		NAME_FIELD));
            add(new TypeElementProperty(PHONE_FIELD, 						ContentType.String,		PHONE_FIELD));
            add(new TypeElementProperty(EMAIL_FIELD, 						ContentType.String,		EMAIL_FIELD));
            add(new TypeElementProperty(ASSIGNED_USER_FIELD, 				ContentType.Boolean,	ASSIGNED_USER_FIELD));
            add(new TypeElementProperty(ONBOARDING_PHONE_FIELD, 			ContentType.String,		ONBOARDING_PHONE_FIELD));
            add(new TypeElementProperty(CARRIER_UNDERWRITING_PHONE_FIELD,	ContentType.String,		CARRIER_UNDERWRITING_PHONE_FIELD));
        }};
    }
}
